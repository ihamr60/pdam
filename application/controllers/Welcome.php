<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Created by Ilham Ramadhan
	 */

	private $username = 'pdamlangsa';
    private $password = 'Reed6mee';
    private $endpoint = 'https://api1.simada.co.id/';

	public function __construct(){
        parent::__construct();
    }
    
    public function requestGet($path, $fields = [])
	{
        $postvars = '/?api_key=DA4Q4IMK5XI67TKF2RGD04NBKNCP0F1OR2ODMAPF&';
        foreach($fields as $key=>$value) {
          $postvars .= "{$key}={$value}&";
        }

		$url = "{$this->endpoint}{$path}{$postvars}";
		
		$credentials = base64_encode("{$this->username}:{$this->password}");
		
		$headers = [];
		$headers[] = "Authorization: Basic {$credentials}";
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		$headers[] = 'Cache-Control: no-cache';
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
            return 'Error:' . curl_error($ch);
            curl_close($ch);
		}else{
            curl_close($ch);
            return json_decode($result , true);
        }
    }

	public function index()
	{
		$bc['data_desa'] 	= $this->web_app_model->getAllData('tbl_desa');	
		$this->load->view('landing',$bc);
		//$this->load->view('maintenance',$bc);
	}

	public function bg_utama()
	{

		if (isset($_GET['bg_complaintPelanggan']) && $_GET['bg_complaintPelanggan']==1) 
		{
			$this->session->set_flashdata("bg-complaintPelanggan-active","tab-active");
			$this->session->set_flashdata("bg-complaintPelanggan-link-active","tab-link-active");
		}

		if (isset($_GET['bg_home']) && $_GET['bg_home']==1) 
		{
			$this->session->set_flashdata("bg-home-active","tab-active");
			$this->session->set_flashdata("bg-home-link-active","tab-link-active");
			$this->session->set_flashdata("pop-up","<button style='display: none;' class='but' onclick='on()'>Pop Up Opening</button>");
			
		}

		if (isset($_GET['bg_status']) && $_GET['bg_status']==1) 
		{
			$this->session->set_flashdata("bg-status-active","tab-active");
			$this->session->set_flashdata("bg-status-link-active","tab-link-active");
		}

		if (isset($_GET['bg_cekTagihan']) && $_GET['bg_cekTagihan']==1) 
		{
			$this->session->set_flashdata("bg-cekTagihan-active","tab-active");
			$this->session->set_flashdata("bg-cekTagihan-link-active","tab-link-active");
		}

		if (isset($_GET['bg_pasangBaru']) && $_GET['bg_pasangBaru']==1) 
		{
			$this->session->set_flashdata("bg-pasangBaru-active","tab-active");
			$this->session->set_flashdata("bg-pasangBaru-link-active","tab-link-active");
		}

		
		$bc['data_desa'] 				= $this->web_app_model->getAllData('tbl_desa');
		$bc['data_kecamatan'] 			= $this->web_app_model->getAllData('tbl_kecamatan');
		$bc['data_tips'] 				= $this->web_app_model->getWhereAllItem('Tips','kategori_artikel','app_artikel');
		$bc['data_story'] 				= $this->web_app_model->getWhereAllItem('Story','kategori_artikel','app_artikel');
		$bc['data_allArtikel'] 			= $this->web_app_model->getAllData('app_artikel');

		$bc['data_complaint'] 			= $this->web_app_model->getAllData('app_complaint');
		$bc['info_gangguan'] 			= $this->web_app_model->getMaxIdWhere('app_gangguan','id_gangguan','1','stts_gangguan');
		
		$bc['menuAtas_pengaduan'] 		= $this->load->view('page/menuAtas_pengaduan','',true);
		//$bc['tableStyle'] 				= $this->load->view('js-css/tableStyle','',true);
		$bc['googleMap'] 				= $this->load->view('js-css/googleMap','',true);
		$bc['preLoader'] 				= $this->load->view('js-css/preLoader','',true);
		$bc['previewCamera'] 			= $this->load->view('js-css/previewCamera','',true);
		$bc['previewCamera2'] 			= $this->load->view('js-css/previewCamera2','',true);
		$bc['previewCamera3'] 			= $this->load->view('js-css/previewCamera3','',true);
		$bc['previewCamera4'] 			= $this->load->view('js-css/previewCamera4','',true);
		$bc['menuBawah'] 				= $this->load->view('menuBawah','',true);
		$bc['bg_cekTagihan'] 			= $this->load->view('page/bg_cekTagihan',$bc,true);
		$bc['bg_pasangBaru'] 			= $this->load->view('page/bg_pasangBaru',$bc,true);
		$bc['bg_home'] 					= $this->load->view('page/bg_home',$bc,true);
		$bc['bg_post'] 					= $this->load->view('page/bg_post',$bc,true);
		$bc['bg_petaLoket'] 			= $this->load->view('page/bg_petaLoket','',true);
		$bc['bg_pengaduan'] 			= $this->load->view('page/bg_pengaduan',$bc,true);
		$bc['bg_menuComplaint'] 		= $this->load->view('page/bg_menuComplaint','',true);
		$bc['bg_status'] 				= $this->load->view('page/bg_status',$bc,true);
		$bc['bg_pengaduanPelanggan'] 	= $this->load->view('page/bg_pengaduanPelanggan',$bc,true);

		$this->load->view('bg_utama',$bc);
	}

	public function addComplaint()
	{
		date_default_timezone_set('Asia/Jakarta');
		$getIdReport 			= $this->web_app_model->getMaxIdReport('app_complaint','id_report');
		$id_report				= $getIdReport['id_report']+1;

		$id_pelapor				= $this->input->post('id_pelapor');
		//$data_pelanggan 		= $this->web_app_model->getWhereOneItem_db2($id_pelapor,'id_pelanggan','tbl_pelanggan'); 
		$data_pelanggan 		= $this->web_app_model->getWhereOneItem($id_pelapor,'id_pelanggan','tbl_pelanggan');
		$nama_pelapor			= $data_pelanggan['nama_pelanggan'];

		$uuid_complaint			= $this->get_id();
		$hp_pelapor				= $this->input->post('hp_pelapor');
		$email_pelapor			= $this->input->post('email_pelapor');
		$kateg_complaint		= $this->input->post('kateg_complaint');
		$isi_laporan			= $this->input->post('isi_laporan');
		$lat_complaint			= $this->input->post('lat_complaint');
		$lng_complaint			= $this->input->post('lng_complaint');
		$stts_complaint			= 'Pending';
		$complaint_updated_at	= date('Y-m-d H:i:s');
		
		// get foto
		 $config['upload_path'] 	= './upload/complaint';
		 $config['allowed_types'] 	= 'jpg|png|jpeg|gif|pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $uuid_complaint;

     	 $this->load->library('upload', $config);

		if(!empty($nama_pelapor))
		{
			if(!empty($_FILES['attach']['name'])) 
			{
		        if ( $this->upload->do_upload('attach') ) {
		            $foto = $this->upload->data();			
					$data = array(
								
						'uuid_complaint' 		=> $uuid_complaint,
						'id_pelapor' 			=> $id_pelapor,
						'nama_pelapor' 			=> $nama_pelapor,
						'id_report' 			=> $id_report,
						'hp_pelapor' 			=> $hp_pelapor,
						'email_pelapor' 		=> $email_pelapor,
						'kateg_complaint' 		=> $kateg_complaint,
						'isi_laporan' 			=> $isi_laporan,
						'lat_complaint' 		=> $lat_complaint,
						'lng_complaint' 		=> $lng_complaint,
						'stts_complaint' 		=> $stts_complaint,
						'stts_pelapor' 			=> 'Pelanggan',
						'complaint_updated_at' 	=> $complaint_updated_at,
						'attach' 				=> $foto['file_name'],				
					);
			
					$this->web_app_model->insertData($data,'app_complaint');
					header('location:'.base_url().'index.php/welcome/bg_utama?bg_status=1');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success! - RPT-$id_report',
										                text:  'Pengaduan Anda telah kami terima, harap catat ID Report berikut untuk memeriksa status pengaduan Anda. ID Report Anda: $id_report',
										                type: 'success',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");

						$pendingPelanggan		= $this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
						$processPelanggan		= $this->web_app_model->getCount2Where('app_complaint','stts_complaint','Process','stts_pelapor','Pelanggan');
						$pendingNonPelanggan	= $this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
						$processNonPelanggan	= $this->web_app_model->getCount2Where('app_complaint','stts_complaint','Process','stts_pelapor','Non-pelanggan');

						$time 	= date('l, d F Y | H:i');

						$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
						$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
						$pesan 	= "<b>[LAPORAN MASUK]</b>\n".$time." WIB\n\n1 Laporan Pelanggan diterima a.n ".$nama_pelapor.". Berikut kondisi terkini:\n\n<b>Laporan Pelanggan</b>\nPending: <b>".$pendingPelanggan['TOTAL']."</b> Kasus\nOn Process: <b>".$processPelanggan['TOTAL']."</b> Kasus \n\n<b>Laporan Masyarakat</b>\nPending: <b>".$pendingNonPelanggan['TOTAL']."</b> Kasus\nOn Process: <b>".$processNonPelanggan['TOTAL']."</b> Kasus";

						// ----------- code -------------

						$method	= "sendMessage";
						$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
						$post = [
						 'chat_id' => $chatid,
						  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
						 'text' => $pesan
						];

						$header = [
						 "X-Requested-With: XMLHttpRequest",
						 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
						];

						// hapus 1 baris ini:
						//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


						$ch = curl_init();
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_URL, $url);
						//curl_setopt($ch, CURLOPT_REFERER, $refer);
						//curl_setopt($ch, CURLOPT_VERBOSE, true);
						curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						$datas = curl_exec($ch);
						$error = curl_error($ch);
						$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);

						$debug['text'] = $pesan;
						$debug['code'] = $status;
						$debug['status'] = $error;
						$debug['respon'] = json_decode($datas, true);
				}
				else 
					{
		          	header('location:'.base_url().'index.php/welcome/bg_utama?bg_complaintPelanggan=1');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Ups, Sorry!',
										                text:  'Pengaduan anda gagal terkirim, coba lagi atau datang langsung ke PDAM terkait kesalahan ini',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
				{
	          	header('location:'.base_url().'index.php/welcome/bg_utama?bg_complaintPelanggan=1');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Lampiran kosong!',
									                text:  'Mohon lampirkan sebuah foto yang dapat mendukung kami untuk menggambarkan situasi Anda',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    	}
		}
		else
		{
			header('location:'.base_url().'index.php/welcome/bg_utama?bg_complaintPelanggan=1');
			$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Sorry!',
										                text:  'ID Pelanggan tidak tersedia, jika Anda tidak memiliki ID Pelanggan silahkan gunakan fasilitas pengaduan Non-Pelanggan.',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
		} 
	}

	public function addComplaintNonPelanggan()
	{

		date_default_timezone_set('Asia/Jakarta');
		$getIdReport 			= $this->web_app_model->getMaxIdReport('app_complaint','id_report');
		$id_report				= $getIdReport['id_report']+1;

		$id_pelapor				= $this->input->post('id_pelapor');
		//$data_pelanggan 		= $this->web_app_model->getWhereOneItem($id_pelapor,'id_pelanggan','tbl_pelanggan_tmp');
		$nama_pelapor			= $this->input->post('nama_pelapor');

		$uuid_complaint			= $this->get_id();
		$hp_pelapor				= $this->input->post('hp_pelapor');
		$email_pelapor			= $this->input->post('email_pelapor');
		$kateg_complaint		= $this->input->post('kateg_complaint');
		$isi_laporan			= $this->input->post('isi_laporan');
		$lat_complaint			= $this->input->post('lat_complaint');
		$lng_complaint			= $this->input->post('lng_complaint');
		$stts_complaint			= 'Pending';
		$complaint_updated_at	= date('Y-m-d H:i:s');
		
		// get foto
		 $config['upload_path'] 	= './upload/complaint';
		 $config['allowed_types'] 	= 'jpg|png|jpeg|gif|pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $uuid_complaint;

     	 $this->load->library('upload', $config);

		if(!empty($_FILES['attach']['name'])) 
		{
	        if ( $this->upload->do_upload('attach') ) {
	            $foto = $this->upload->data();			
				$data = array(
							
					'uuid_complaint' 		=> $uuid_complaint,
					'id_pelapor' 			=> $id_pelapor,
					'nama_pelapor' 			=> $nama_pelapor,
					'id_report' 			=> $id_report,
					'hp_pelapor' 			=> $hp_pelapor,
					'email_pelapor' 		=> $email_pelapor,
					'kateg_complaint' 		=> $kateg_complaint,
					'isi_laporan' 			=> $isi_laporan,
					'lat_complaint' 		=> $lat_complaint,
					'lng_complaint' 		=> $lng_complaint,
					'stts_complaint' 		=> $stts_complaint,
					'stts_pelapor' 			=> 'Non-pelanggan',
					'complaint_updated_at' 	=> $complaint_updated_at,
					'attach' 				=> $foto['file_name'],					
				);
		
				$this->web_app_model->insertData($data,'app_complaint');
				header('location:'.base_url().'index.php/welcome/bg_utama?bg_status=1');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Success! - RPT-$id_report',
									                text:  'Pengaduan Anda telah kami terima, harap catat ID Report berikut untuk memeriksa status pengaduan Anda. ID Report Anda: $id_report',
									                type: 'success',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");

				$pendingPelanggan		= $this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
				$processPelanggan		= $this->web_app_model->getCount2Where('app_complaint','stts_complaint','Process','stts_pelapor','Pelanggan');
				$pendingNonPelanggan	= $this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
				$processNonPelanggan	= $this->web_app_model->getCount2Where('app_complaint','stts_complaint','Process','stts_pelapor','Non-pelanggan');

				$time 	= date('l, d F Y | H:i');

				$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
				$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
				$pesan 	= "<b>[LAPORAN MASUK]</b>\n".$time." WIB\n\n1 Laporan Masyarakat diterima a.n ".$nama_pelapor.". Berikut kondisi terkini:\n\n<b>Laporan Pelanggan</b>\nPending: <b>".$pendingPelanggan['TOTAL']."</b> Kasus\nOn Process: <b>".$processPelanggan['TOTAL']."</b> Kasus \n\n<b>Laporan Masyarakat</b>\nPending: <b>".$pendingNonPelanggan['TOTAL']."</b> Kasus\nOn Process: <b>".$processNonPelanggan['TOTAL']."</b> Kasus";

				// ----------- code -------------

				$method	= "sendMessage";
				$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
				$post = [
				 'chat_id' => $chatid,
				  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
				 'text' => $pesan
				];

				$header = [
				 "X-Requested-With: XMLHttpRequest",
				 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
				];

				// hapus 1 baris ini:
				//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				//curl_setopt($ch, CURLOPT_REFERER, $refer);
				//curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$datas = curl_exec($ch);
				$error = curl_error($ch);
				$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);

				$debug['text'] = $pesan;
				$debug['code'] = $status;
				$debug['status'] = $error;
				$debug['respon'] = json_decode($datas, true);
			}
			else 
				{
	          	header('location:'.base_url().'index.php/welcome/bg_utama?bg_complaintPelanggan=1');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Ups, Sorry!',
									                text:  'Pengaduan anda gagal terkirim, coba lagi atau datang langsung ke PDAM terkait kesalahan ini',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
	    	}
	    }
	    else 
		{
      	header('location:'.base_url().'index.php/welcome/bg_utama?bg_complaintPelanggan=1');
		$this->session->set_flashdata("info2","<script type='text/javascript'>
							     setTimeout(function () { 
							     swal({
							                title: 'Lampiran kosong!',
							                text:  'Mohon lampirkan sebuah foto yang dapat mendukung kami untuk menggambarkan situasi Anda',
							                type: 'warning',
							                timer: 300000,
							                showConfirmButton: true
							            });  
							     },10);  
							    </script>");
    	}
	}


	public function cekStatus()
	{
		$id_report				= $this->input->post('id_report');
		$hp_pelapor				= $this->input->post('hp_pelapor');
		$cekIdReport 			= $this->web_app_model->getWhereTwoItem($id_report,'id_report',$hp_pelapor,'hp_pelapor','app_complaint');
		$nama_pelapor			= $cekIdReport['nama_pelapor'];
		
		if(!empty($cekIdReport['stts_complaint']) && $cekIdReport['stts_complaint']=='Pending')
		{	
			header('location:'.base_url().'index.php/welcome/bg_utama?bg_status=1');
			$this->session->set_flashdata("info2","<script type='text/javascript'>
								     setTimeout(function () { 
								     swal({
								                title: 'Status Pending!',
								                text:  'Hi $nama_pelapor, status pengaduan Anda saat ini masih dalam antrian. Silahkan kembali lagi besok atau beberapa saat lagi',
								                type: 'success',
								                timer: 300000,
								                showConfirmButton: true
								            });  
								     },10);  
								    </script>");
			
		 
		   
		} 
		else if(!empty($cekIdReport['stts_complaint']) && $cekIdReport['stts_complaint']=='Process')
		{	
			header('location:'.base_url().'index.php/welcome/bg_utama?bg_status=1');
			$this->session->set_flashdata("info2","<script type='text/javascript'>
								     setTimeout(function () { 
								     swal({
								                title: 'Status On Process!',
								                text:  'Hi $nama_pelapor, status pengaduan Anda saat ini sedang dalam prosess pengambilan kebijakan. Pada tahap ini petugas kami mungkin sudah / akan segera menghubungi Anda melalui No HP yang Anda gunakan saat melapor untuk tindakan lanjutan. Semoga layanan kami dapat membantu. Terimakasih!',
								                type: 'success',
								                timer: 300000,
								                showConfirmButton: true
								            });  
								     },10);  
								    </script>");
			
		 
		   
		}
		else if(!empty($cekIdReport['stts_complaint']) && $cekIdReport['stts_complaint']=='Clear')
		{	
			header('location:'.base_url().'index.php/welcome/bg_utama?bg_status=1');
			$this->session->set_flashdata("info2","<script type='text/javascript'>
								     setTimeout(function () { 
								     swal({
								                title: 'Status Clear!',
								                text:  'Hi $nama_pelapor, status pengaduan Anda saat ini sudah selesai dan sudah ditutup oleh sistem. Terimakasih telah menggunakan layanan kami. Semoga hari Anda menyenangkan!',
								                type: 'success',
								                timer: 300000,
								                showConfirmButton: true
								            });  
								     },10);  
								    </script>");
			
		 
		   
		}
		else
		{
			header('location:'.base_url().'index.php/welcome/bg_utama?bg_status=1');
			$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Not Found!',
										                text:  'Kombinasi ID Report & No Hp tidak ditemukan, mohon periksa kembali',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
		} 
	}

	public function cekTagihan()
	{
	
        function rupiah($angka){
		
			$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
			return $hasil_rupiah;
		 
		}


		//EDITABLE
		$table 					= 'tbl_drd_fix';
		$id_pelanggan			= $this->input->post('id_pelanggan');
		$drd_pelanggan 			= $this->web_app_model->getWhereAllItem_db2($id_pelanggan,'id_pelanggan',$table);


		//Select Tanggal Terakhir Update
		$last_updated			= $this->web_app_model->getLimitOneItem_db2('tgl_cetak','tgl_cetak',$table);



		if(count($drd_pelanggan->result())>0)
		{	
			
			$n 				= 1;
			$tunggakan 		= 0;
			$totBln_nunggak = 0;

			foreach($drd_pelanggan->result_array() as $d)
			{
				date_default_timezone_set('Asia/Jakarta');
				$due_date				= strtotime($d["due_date"]);
				$tgl 					= strtotime(date('Y-m-d'));

			

				//DENDA ========================================================
				if($due_date<$tgl && date('Y-m', $tgl)!=date('Y-m', $due_date))
				{
					$denda 				= '0';
				}
				else if($due_date<$tgl && date('Y-m', $tgl)==date('Y-m', $due_date))
				{
					$denda 				= '7000';
				}
				//==============================================================

				//=========================================================
				if($d["tercetak"]==1)
				{
					$stts 				= "LUNAS";
					$tunggak_bln   		= 0;
				}
				else
				{
					$stts 				= "BELUM LUNAS";
					//$tunggak_bln 		= $d["harga_air"]+$d["harga_air_koreksi"]+$d["badmin"]+$d["bmeter"]+$d["terlambat"]+$denda;
					$tunggak_bln 		= $d["harga_air"]+$d["harga_air_koreksi"]+$d["badmin"]+$d["bmeter"]+$d["terlambat"];
					$totBln_nunggak++;
				}

				//$total 					= $d["harga_air"]+$d["harga_air_koreksi"]+$d["badmin"]+$d["bmeter"]+$d["terlambat"]+$denda;
				$total 					= $d["harga_air"]+$d["harga_air_koreksi"]+$d["badmin"]+$d["bmeter"]+$d["terlambat"];
				$tunggakan   			= $tunggakan + $tunggak_bln;
				//=============================================================


				// Informasi Last Updated
				$this->session->set_flashdata("last_updated","<b>Terakhir diupdate ".date('d-m-Y', strtotime($last_updated['tgl_cetak'])).".</b>");
				
				if($d['tercetak']!=1)
				{

					$this->session->set_flashdata("info2","<script type='text/javascript'>
								     setTimeout(function () { 
								     swal({
								                title: 'Tunggakan ditemukan!',
								                text:  'Terakhir diupdate ".date('d-F-Y', strtotime($last_updated['tgl_cetak'])).", Pukul ".date('H:i:s', strtotime($last_updated['tgl_cetak']))." WIB',
								                type: 'info',
								                timer: 40500,
								                showConfirmButton: true
								            });  
								     },10);  
								    </script>");

					$this->session->set_flashdata("infoTagihan".$n."","
					<div class='container animated fadeIn'>
					            <div class='row'>
					              <div class='column left'>
					                <span class='dot' style='background:#ED594A;'></span>
					                <span class='dot' style='background:#5AC05A;'></span>
					              </div>
					              <div class='column middle'>
					                <b>Tagihan Periode ".$d["periode"]."</b>
					              </div>					              
					            </div>

					            <div class='content'>
	                                <table class='table table-striped'>
	                                  <tr>
	                                    <th width='20%'' align='left'>ID Pelanggan</th>
	                                    <td width='1%''>:</td>
	                                    <td>".$d["id_pelanggan"]."</td>
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Nama</th>
	                                    <td>:</td>
	                                    <td>".$d["nama_pelanggan"]."</td>
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Periode</th>
	                                    <td>:</td>
	                                    <td>".date('F-Y', strtotime('-1 month', strtotime($d['due_date'])))."</td>
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Biaya Air</th>
	                                    <td>:</td>
	                                    <td>".rupiah($d["harga_air"])."</td>
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Koreksi (Air)</th>
	                                    <td>:</td>
	                                    <td>".rupiah($d["harga_air_koreksi"])."</td>
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Biaya Admin</th>
	                                    <td>:</td>
	                                    <td>".rupiah($d["badmin"])."</td>
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Biaya Meter</th>
	                                    <td>:</td>
	                                    <td>".rupiah($d["bmeter"])."</td>
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Denda</th>
	                                    <td>:</td>
	                                    <td>".rupiah($d["terlambat"])/*rupiah($d["terlambat"]+$denda)*/."</td>
	                                  </tr>
	                                  
	                                  <tr>
	                                    <th align='left'>===========</th>
	                                    
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Jml. Total</th>
	                                    <td>:</td>
	                                    <td>".rupiah($total)."</td>
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Status</th>
	                                    <td>:</td>
	                                    <td>".$stts."</td>
	                                  </tr> 
	                                  <tr>
	                                    <th align='left'>===========</th>
	                                    
	                                  </tr>
	                                  <tr>
	                                    <th align='left'>Tunggakan</th>
	                                    <td>:</td>
	                                    <td>".rupiah($tunggakan)." (".$totBln_nunggak." bln)</td>
	                                  </tr>	                                  
	                                </table>	                                
					            </div>
					          </div>
					    	<br>
					          ");
					$n++;
				}
				else if($d['tercetak']==1)
				{
					$this->session->set_flashdata("infoLunas","<script type='text/javascript'>
								     setTimeout(function () { 
								     swal({
								                title: 'Lunas!',
								                text:  'Hi ".$d["nama_pelanggan"].", Anda tidak memiliki tagihan menunggak pada bulan ".date('F-Y', strtotime('-1 month', strtotime($d['due_date']))).". Untuk info tagihan bulan berikutnya akan terupdate pada tanggal 2-3 pada bulan berikutnya. Terima kasih :)',
								                type: 'success',
								                timer: 40500,
								                showConfirmButton: true
								            });  
								     },10);  
								    </script>");
				}
			}
			
		header('location:'.base_url().'index.php/welcome/bg_utama?bg_cekTagihan=1');
	
		} 
		else
		{	
			header('location:'.base_url().'index.php/welcome/bg_utama?bg_cekTagihan=1');
			$this->session->set_flashdata("info2","<script type='text/javascript'>
								     setTimeout(function () { 
								     swal({
								                title: 'Failed!',
								                text:  'Data tidak ditemukan, coba lagi',
								                type: 'warning',
								                timer: 8000,
								                showConfirmButton: true
								            });  
								     },10);  
								    </script>");
		}
	}

	public function cekTagihanAPI()
	{

		$idp 		= $this->input->post('id_pelanggan');
    	$lunas 		= '0';
        $response 		= $this->requestGet('/api/checktag/tagihan', ['idp'=>$idp, 'lunas' => $lunas]);

       // print_r($data);

		//START DETEKSI AKTIFITAS =============================

		$data = array(

					'log_activity' 			=> "ID Pel: ".$idp,
					'log_kateg' 			=> "Cek Tagihan",				
				);
		
		$this->web_app_model->insertData($data,'app_activity_log');
		

		//END DETEKSI AKTIFITAS ================================



		
	
        function rupiah($angka){
		
			$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
			return $hasil_rupiah;
		 
		}


	

					$this->session->set_flashdata("info2","<script type='text/javascript'>
								     setTimeout(function () { 
								     swal({
								                title: 'Data Tunggakan ditemukan!',
								                text:  'Menunggak selama 3 bulan akan diberikan surat pemberitahuan. Jika tidak segera ditindaklanjuti, maka akan dilakukan pemutusan.',
								                type: 'info',
								                timer: 40500,
								                showConfirmButton: true
								            });  
								     },10);  
								    </script>");
		if(!empty($response['status']))
		{
			$n =1;
			foreach($response['data'] as $d)
			{

				$this->session->set_flashdata("infoTagihan".$n."","
				    	<div class='container animated fadeIn'>
				            <div class='row'>
				              <div class='column left'>
				                <span class='dot' style='background:#ED594A;'></span>
				                <span class='dot' style='background:#5AC05A;'></span>
				              </div>
				              <div class='column middle'>
				                <b>Tagihan Periode ".$d["periode"]."</b>
				              </div>					              
				            </div>

				            <div class='content'>
	                            <table class='table table-striped'>
	                              <tr>
	                                <th width='20%'' align='left'>ID Pelanggan</th>
	                                <td width='1%''>:</td>
	                                <td>".$d["idp"]."</td>
	                              </tr>
	                              <tr>
	                                <th align='left'>Nama</th>
	                                <td>:</td>
	                                <td>".$d["nama"]."</td>
	                              </tr>
	                              <tr>
	                                <th align='left'>Periode</th>
	                                <td>:</td>
	                                <td>".$d['periode']."</td>
	                              </tr>
	                              <tr>
	                                <th align='left'>Gol. Tarif</th>
	                                <td>:</td>
	                                <td>".$d['goltarif']."</td>
	                              </tr>
	                              <tr>
	                                <th align='left'>Alamat</th>
	                                <td>:</td>
	                                <td>".$d['alamat']."</td>
	                              </tr>
	                              <tr>
	                                <th align='left'>Biaya Air</th>
	                                <td>:</td>
	                                <td>".rupiah($d["hargaair"])."</td>
	                              </tr>
	                              <tr>
	                                <th align='left'>Biaya Admin</th>
	                                <td>:</td>
	                                <td>".rupiah($d["badmin"])."</td>
	                              </tr>
	                              <tr>
	                                <th align='left'>Biaya Meter</th>
	                                <td>:</td>
	                                <td>".rupiah($d["bmeter"])."</td>
	                              </tr>
	                              <tr>
	                                <th align='left'>Denda</th>
	                                <td>:</td>
	                                <td>".rupiah($d["denda"])/*rupiah($d["terlambat"]+$denda)*/."</td>
	                              </tr>
	                              
	                              <tr>
	                                <th align='left'>===========</th>
	                                
	                              </tr>
	                              <tr>
	                                <th align='left'>Jml. Total</th>
	                                <td>:</td>
	                                <td>".rupiah($d['jumlah'])."</td>
	                              </tr>
	                              <tr>
	                                <th align='left'>Status</th>
	                                <td>:</td>
	                                <td>BELUM LUNAS</td>
	                              </tr> 
	                              <tr>
	                                <th align='left'>===========</th>
	                              </tr>                           
	                            </table>
	                            <font size=0>*Tagihan diatas belum termasuk biaya adm. bank</font>	                                
				            </div>
				          </div>
				    	<br>
				          ");
				$n++;
			}
			
			header('location:'.base_url().'index.php/welcome/bg_utama?bg_cekTagihan=1');
		}
		else
		{
			header('location:'.base_url().'index.php/welcome/bg_utama?bg_cekTagihan=1');
			$this->session->set_flashdata("info2","<script type='text/javascript'>
								     setTimeout(function () { 
								     swal({
								                title: 'Tidak ada Tunggakan!',
								                text:  'Data Tunggakan dengan ID $idp pada bulan berjalan tidak ditemukan dalam database kami. Terimakasih telah membayar tepat waktu :)',
								                type: 'warning',
								                timer: 40000,
								                showConfirmButton: true
								            });  
								     },10);  
								    </script>");
		}
	}

	public function addPasangBaru()
	{

		date_default_timezone_set('Asia/Jakarta');
		$getIdReg 				= $this->web_app_model->getMaxIdReg('app_pasangBaru','id_reg');
		$id_reg					= $getIdReg['id_reg']+1;

		$ktp_pemohon			= $this->input->post('ktp_pemohon');
		$nama_pemohon			= $this->input->post('nama_pemohon');

		$uuid_pasang_baru		= $this->get_id();

		$hp_pemohon				= $this->input->post('hp_pemohon');
		$id_desa_pemohon		= $this->input->post('id_desa_pemohon');
		//$id_kec_pemohon			= $this->input->post('id_kec_pemohon');
		$alamat_pemohon			= $this->input->post('alamat_pemohon');
		$lat_pemohon			= $this->input->post('lat_pemohon');
		$lng_pemohon			= $this->input->post('lng_pemohon');
		$img_ktp				= $this->input->post('img_ktp');

		$id_kec_pemohon 		= $this->web_app_model->getWhereOneItem($id_desa_pemohon,'DesaID','tbl_desa');

		$stts_pasang_baru		= 'Pending';
		$created_at_pasang_baru	= date('Y-m-d H:i:s');
		
		// get foto
		 $config['upload_path'] 	= './upload/pasang_baru';
		 $config['allowed_types'] 	= 'jpg|png|jpeg|gif|pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $uuid_pasang_baru;

     	 $this->load->library('upload', $config);

		if(!empty($_FILES['img_ktp']['name'])) 
		{
	        if ( $this->upload->do_upload('img_ktp') ) {
	            $foto = $this->upload->data();			
				$data = array(
					'id_reg' 				=> $id_reg,
					'uuid_pasang_baru' 		=> $uuid_pasang_baru,
					'ktp_pemohon' 			=> $ktp_pemohon,
					'nama_pemohon' 			=> $nama_pemohon,
					'hp_pemohon' 			=> $hp_pemohon,
					'id_desa_pemohon' 		=> $id_desa_pemohon,
					'id_kec_pemohon' 		=> $id_kec_pemohon['KecamatanID'],
					'alamat_pemohon' 		=> $alamat_pemohon,
					'lat_pemohon' 			=> $lat_pemohon,
					'lng_pemohon' 			=> $lng_pemohon,
					'img_ktp' 				=> $foto['file_name'],
					'stts_pasang_baru' 		=> $stts_pasang_baru,
					'created_at_pasang_baru'=> $created_at_pasang_baru,					
				);
		
				$this->web_app_model->insertData($data,'app_pasangBaru');
				header('location:'.base_url().'index.php/welcome/bg_utama?bg_pasangBaru=1');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Success! - REG-$id_reg',
									                text:  'Permohonan Anda telah kami terima, silahkan menunggu Petugas kami menghubungi Anda dalam 1 x 24 jam. ID Registrasi Anda: $id_reg',
									                type: 'success',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");

				$pendingPasangBaru		= $this->web_app_model->getCount('app_pasangBaru','stts_pasang_baru','Pending');
				$processPasangBaru		= $this->web_app_model->getCount('app_pasangBaru','stts_pasang_baru','Process');
				$instalasiPasangBaru	= $this->web_app_model->getCount('app_pasangBaru','stts_pasang_baru','Instalasi');

				$time 	= date('l, d F Y');

				$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
				$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
				$pesan 	= "<b>[TIKET PASANG BARU]</b>\n".$time."\n\n1 Permohonan Pasang Baru diterima a.n ".$nama_pemohon.". Berikut kondisi terkini:\n\n<b>Status Pemasangan Baru</b>\nPending: <b>".$pendingPasangBaru['TOTAL']."</b> Request\nOn Survey: <b>".$processPasangBaru['TOTAL']."</b> Request\nOn Instalation: <b>".$instalasiPasangBaru['TOTAL']."</b> Request";

				// ----------- code -------------

				$method	= "sendMessage";
				$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
				$post = [
				 'chat_id' => $chatid,
				  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
				 'text' => $pesan
				];

				$header = [
				 "X-Requested-With: XMLHttpRequest",
				 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
				];

				// hapus 1 baris ini:
				//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				//curl_setopt($ch, CURLOPT_REFERER, $refer);
				//curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$datas = curl_exec($ch);
				$error = curl_error($ch);
				$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);

				$debug['text'] = $pesan;
				$debug['code'] = $status;
				$debug['status'] = $error;
				$debug['respon'] = json_decode($datas, true);
			}
			else 
				{
	          	header('location:'.base_url().'index.php/welcome/bg_utama?bg_pasangBaru=1');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Ups, Maaf!',
									                text:  'Permohonan anda gagal terkirim, coba lagi atau datang langsung ke PDAM terkait kesalahan ini',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
	    	}
	    }
	    else 
		{
      	header('location:'.base_url().'index.php/welcome/bg_utama?bg_pasangBaru=1');
		$this->session->set_flashdata("info2","<script type='text/javascript'>
							     setTimeout(function () { 
							     swal({
							                title: 'Lampiran kosong!',
							                text:  'Mohon lampirkan Foto KTP Anda',
							                type: 'warning',
							                timer: 300000,
							                showConfirmButton: true
							            });  
							     },10);  
							    </script>");
    	}
	}
}
