<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdam_Api extends CI_Controller {

    private $username = 'pdamlangsa';
    private $password = 'Reed6mee';
    private $endpoint = 'https://api1.simada.co.id/';

	public function __construct(){
        parent::__construct();
    }
    
    public function requestGet($path, $fields = [])
	{
        $postvars = '/?api_key=DA4Q4IMK5XI67TKF2RGD04NBKNCP0F1OR2ODMAPF&';
        foreach($fields as $key=>$value) {
          $postvars .= "{$key}={$value}&";
        }

		$url = "{$this->endpoint}{$path}{$postvars}";
		
		$credentials = base64_encode("{$this->username}:{$this->password}");
		
		$headers = [];
		$headers[] = "Authorization: Basic {$credentials}";
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		$headers[] = 'Cache-Control: no-cache';
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
            return 'Error:' . curl_error($ch);
            curl_close($ch);
		}else{
            curl_close($ch);
            return json_decode($result , true);
        }
    }
    
    public function requestPost($path, $fields = [])
	{
        $postvars = '';
        foreach($fields as $key=>$value) {
          $postvars .= "{$key}={$value}&";
        }

		$url = "{$this->endpoint}{$path}";
		
		$credentials = base64_encode("{$this->username}:{$this->password}");
		
		$headers = [];
		$headers[] = "Authorization: Basic {$credentials}";
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		$headers[] = 'Cache-Control: no-cache';
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
            return 'Error:' . curl_error($ch);
            curl_close($ch);
		}else{
            curl_close($ch);
            return json_decode($result , true);
        }
    }

    public function getKecamatan()
    {
        $data = $this->requestGet('/api/checktag/kecamatan');

        print_r($data);
    }

    public function getDesa($kec_id)
    {
        $data = $this->requestGet('/api/checktag/desa', ['id'=>$kec_id]);

        print_r($data);
    }

    public function getTagihan()
    {
    	$idp 		= $this->uri->segment(3);
    	$lunas 		= $this->uri->segment(4);
        $response 		= $this->requestGet('/api/checktag/tagihan', ['idp'=>$idp, 'lunas' => $lunas]);

        
/*
	    foreach($response['data'] as $d)
	    {
	    	echo "Nama:".$d['nama'];
	    	echo "Jumlah:".$d['jumlah'];
	    	echo "<br>";
	    } */

       print_r($response['data']);
    }

    public function getTagihan2()
    {
    	$idp 		= $this->uri->segment(3);
    	$lunas 		= $this->uri->segment(4);
        $data 		= $this->requestGet('/api/checktag/tagihan', ['idp'=>$idp, 'lunas' => $lunas]);

        
        print_r($data);
    }
    

}