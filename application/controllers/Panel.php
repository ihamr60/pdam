<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends MY_Controller {

	/**
	 * Created by Ilham Ramadhan, S.Tr.Kom
	 * Tlp/WA: 0853 6188 5100
	 */

	public function _construct()
	{
		session_start();
	}

	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(empty($cek))
		{
			$this->load->view('panel/login');
		}
		else
		{
			if($stts == 'Administrator')
			{
				header('location:'.base_url().'index.php/panel/bg_home');
			}
		}
	}

	public function login()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->web_app_model->getLoginData($u, $p);
	}
	
	public function logout()
	{
		$cek  = $this->session->userdata('logged_in');
		if(empty($cek))
		{
			header('location:'.base_url().'index.php/panel');
		}
		else
		{
			$this->session->sess_destroy(); // memusnahkan sessionnya
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_home()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			date_default_timezone_set('Asia/Jakarta');
			$year	=	date('Y');
			$month	=	date('m');

			for($bulan=01;$bulan<=12;$bulan++)
			{
				$qreport1	=	$this->web_app_model->getReport($year,$bulan,'Non-pelanggan');
				$qreport2	=	$this->web_app_model->getReport($year,$bulan,'Pelanggan');
				$qreport3	=	$this->web_app_model->getReportPasangBaru($year,$bulan,'Clear');


				$bc['totReport1'][] = $qreport1['TOTAL'];
				$bc['totReport2'][] = $qreport2['TOTAL'];
				$bc['totReport3'][] = $qreport3['TOTAL'];
				if($bulan>=date('m')){
					break;
				}
			}

			//DATA LAPORAN
			$bc['data_laporan'] 	= $this->web_app_model->getWhereAllItem('Pelanggan','stts_pelapor','app_complaint');
			$bc['data_laporanAll'] 	= $this->web_app_model->getAllData('app_complaint');

			// DATA REPORT2 PIECHART
			$bc['meterMacet']		= $this->web_app_model->getCount2Where('app_complaint','kateg_complaint','Meter Macet','month(complaint_updated_at)',$month);
			$bc['kebocoranPipa']	= $this->web_app_model->getCount2Where('app_complaint','kateg_complaint','Kebocoran Pipa','month(complaint_updated_at)',$month);
			$bc['keluhanLainnya']	= $this->web_app_model->getCount2Where('app_complaint','kateg_complaint','Keluhan Lainnya','month(complaint_updated_at)',$month);
			$bc['checkMeteran']		= $this->web_app_model->getCount2Where('app_complaint','kateg_complaint','Check Meteran','month(complaint_updated_at)',$month);
			$bc['bocorDekatMeter']	= $this->web_app_model->getCount2Where('app_complaint','kateg_complaint','Bocor Dekat Meter','month(complaint_updated_at)',$month);
			$bc['airMacet']			= $this->web_app_model->getCount2Where('app_complaint','kateg_complaint','Air Macet','month(complaint_updated_at)',$month);

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======


			$bc['totalPending']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','month(complaint_updated_at)',$month);
			$bc['totalProcess']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Process','month(complaint_updated_at)',$month);
			$bc['totalClear']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Clear','month(complaint_updated_at)',$month);

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['report1'] 			= $this->load->view('graph/report1',$bc,true);
			$bc['report2'] 			= $this->load->view('graph/report2',$bc,true);
			$bc['report3'] 			= $this->load->view('graph/report3',$bc,true);
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_pengumuman()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$bc['info_gangguan'] 	= $this->web_app_model->getMax('app_gangguan','id_gangguan');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_pengumuman',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_lokasiLoket()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_lokasiLoket',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_pasangBaru()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$bc['data_pasangBaru'] 			= $this->web_app_model->getAll3Join('id_desa_pemohon','id_kec_pemohon','DesaID','KecamatanID','app_pasangBaru','tbl_desa','tbl_kecamatan');

			
			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['data_petugas']				= $this->web_app_model->getAllData('app_petugasLap');
			$bc['modalPetugasPasangBaru']	= $this->load->view('panel/modalPetugasPasangBaru',$bc,true);
			$bc['nama'] 					= $this->session->userdata('nama');
			$bc['status'] 					= $this->session->userdata('stts');
			$bc['atas'] 					= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 					= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 						= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_pasangBaru',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_lapPublik()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$bc['data_laporan'] 		= $this->web_app_model->getWhereAllItem('Non-pelanggan','stts_pelapor','app_complaint');

			//$bc['data_laporan'] 		= $this->web_app_model->getJoinAllWhere('pic_complaint','wa','app_complaint','app_petugasLap','stts_pelapor','Non-pelanggan');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['modalDetailKeluhan']	= $this->load->view('panel/modalDetailKeluhan',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_lapPublik',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_lapPelanggan()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$bc['data_laporan'] 		= $this->web_app_model->getWhereAllItem('Pelanggan','stts_pelapor','app_complaint');

			//$bc['data_laporan'] 		= $this->web_app_model->getJoinAllWhere('pic_complaint','wa','app_complaint','app_petugasLap','stts_pelapor','Pelanggan');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['modalDetailKeluhan']	= $this->load->view('panel/modalDetailKeluhan',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_lapPelanggan',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_lapPelanggan_filter()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$tgl_awal					= $this->input->post('from_date');
			$tgl_akhir					= $this->input->post('to_date');

			$bc['data_laporan'] 		= $this->web_app_model->getWhereAllItem_between('Pelanggan','stts_pelapor','app_complaint','complaint_updated_at',$tgl_awal,$tgl_akhir);

			//$bc['data_laporan'] 		= $this->web_app_model->getJoinAllWhere('pic_complaint','wa','app_complaint','app_petugasLap','stts_pelapor','Pelanggan');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['modalDetailKeluhan']	= $this->load->view('panel/modalDetailKeluhan',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_lapPelanggan',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_lapNonPelanggan_filter()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$tgl_awal					= $this->input->post('from_date');
			$tgl_akhir					= $this->input->post('to_date');

			$bc['data_laporan'] 		= $this->web_app_model->getWhereAllItem_between('Non-pelanggan','stts_pelapor','app_complaint','complaint_updated_at',$tgl_awal,$tgl_akhir);

			//$bc['data_laporan'] 		= $this->web_app_model->getJoinAllWhere('pic_complaint','wa','app_complaint','app_petugasLap','stts_pelapor','Pelanggan');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['modalDetailKeluhan']	= $this->load->view('panel/modalDetailKeluhan',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_lapPublik',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_lapPelanggan_report()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$bc['tgl_awal']								= $this->input->post('from_date');
			$bc['tgl_akhir']							= $this->input->post('to_date');

			$bc['data_laporan_all'] 					= $this->web_app_model->getCount_betweenAllReport('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir']);
			$bc['data_laporan_process'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Process');
			$bc['data_laporan_pending'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Pending');
			$bc['data_laporan_selesai'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Clear');

			$bc['data_laporan_airMacet'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'kateg_complaint','Air Macet');
			$bc['data_laporan_cekMeter'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'kateg_complaint','Check Meteran');
			$bc['data_laporan_bocorDekatMeterNmeterMacet'] 		= $this->web_app_model->getCount_betweenReportLaporan_2('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'kateg_complaint','Bocor Dekat Meter','kateg_complaint','Meter Macet');
			$bc['data_laporan_bocorPipa'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'kateg_complaint','Kebocoran Pipa');
			$bc['data_laporan_lainnya'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'kateg_complaint','Keluhan Lainnya');

			$bc['data_laporan_pelanggan'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_pelapor','Pelanggan');
			$bc['data_laporan_nonpelanggan'] 			= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_pelapor','Non-pelanggan');

			/* REPORT SAYED
			$bc['data_laporan_sayedAll'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'nm_pic_complaint','Sayed');
			$bc['data_laporan_sayedProcess'] 			= $this->web_app_model->getCount_betweenReportLaporan_pic('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Process','nm_pic_complaint','Sayed');
			$bc['data_laporan_sayedClear'] 			= $this->web_app_model->getCount_betweenReportLaporan_pic('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Clear','nm_pic_complaint','Sayed');

			*/

			// REPORT SAYED & NANANG
			$bc['data_laporan_perawatanAll'] 			= $this->web_app_model->getCount_betweenReportLaporan_2('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'nm_pic_complaint','Sayed','nm_pic_complaint','Nanang');
			$bc['data_laporan_perawatanProcess'] 		= $this->web_app_model->getCount_betweenReportLaporan_pic_2('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Process','nm_pic_complaint','Sayed','nm_pic_complaint','Nanang');
			$bc['data_laporan_perawatanClear'] 			= $this->web_app_model->getCount_betweenReportLaporan_pic_2('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Clear','nm_pic_complaint','Sayed','nm_pic_complaint','Nanang');

			// REPORT NASIR & HERU
			$bc['data_laporan_nasirAll'] 				= $this->web_app_model->getCount_betweenReportLaporan_2('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'nm_pic_complaint','Nasir','nm_pic_complaint','Heru');
			$bc['data_laporan_nasirProcess'] 			= $this->web_app_model->getCount_betweenReportLaporan_pic_2('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Process','nm_pic_complaint','Nasir','nm_pic_complaint','Heru');
			$bc['data_laporan_nasirClear'] 				= $this->web_app_model->getCount_betweenReportLaporan_pic_2('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Clear','nm_pic_complaint','Nasir','nm_pic_complaint','Heru');

			// REPORT DEPY
			$bc['data_laporan_depyAll'] 				= $this->web_app_model->getCount_betweenReportLaporan('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'nm_pic_complaint','Depy');
			$bc['data_laporan_depyProcess'] 			= $this->web_app_model->getCount_betweenReportLaporan_pic('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Process','nm_pic_complaint','Depy');
			$bc['data_laporan_depyClear'] 				= $this->web_app_model->getCount_betweenReportLaporan_pic('app_complaint','complaint_updated_at',$bc['tgl_awal'],$bc['tgl_akhir'],'stts_complaint','Clear','nm_pic_complaint','Depy');

			//$bc['data_laporan'] 		= $this->web_app_model->getJoinAllWhere('pic_complaint','wa','app_complaint','app_petugasLap','stts_pelapor','Pelanggan');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			//$bc['modalDetailKeluhan']	= $this->load->view('panel/modalDetailKeluhan',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/letter/laporan_keluhan',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_artikel()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$bc['data_artikel'] 		= $this->web_app_model->getAllData('app_artikel');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			//$bc['modalDetailKeluhan']	= $this->load->view('panel/modalDetailKeluhan',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_artikel',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_addArtikel()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			//$bc['modalDetailKeluhan']	= $this->load->view('panel/modalDetailKeluhan',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_addArtikel',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_editArtikel()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$bc['data_artikel'] 		= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'uuid','app_artikel');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			//$bc['modalDetailKeluhan']	= $this->load->view('panel/modalDetailKeluhan',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_editArtikel',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_petugasLap()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$bc['data_petugas'] 		= $this->web_app_model->getAllData('app_petugasLap');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['modalTambahPetugas']	= $this->load->view('panel/modalTambahPetugas',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_petugasLap',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_detailComplaint()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$bc['dataReport'] 			= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'uuid_complaint','app_complaint');

			//==== NOTIFIKASi MENU ======
			$bc['pendingPelanggan']		= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Pelanggan');
			$bc['pendingNonPelanggan']	= 	$this->web_app_model->getCount2Where('app_complaint','stts_complaint','Pending','stts_pelapor','Non-pelanggan');
			$bc['pendingPasangBaru']	= 	$this->web_app_model->getCount1Where('app_pasangBaru','stts_pasang_baru','Pending');
			// === NOTIFIKASI MENU ======
			
			$bc['data_petugas']		= $this->web_app_model->getAllData('app_petugasLap');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['modalAttach']		= $this->load->view('panel/modalAttach',$bc,true);
			$bc['modalPilihPetugas']= $this->load->view('panel/modalPilihPetugas',$bc,true);
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/bg_detailComplaint',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function deleteComplaint()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$uuid_complaint	 		= $this->uri->segment(3);
			$foto 					= $this->uri->segment(4);;
			$hapus 					= array('uuid_complaint'=>$uuid_complaint);
			$path 					= './upload/complaint/';
      		@unlink($path.$foto);
			
			$this->web_app_model->deleteData('app_complaint',$hapus);
			//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
			header('location:'.base_url().'index.php/panel/bg_lapPelanggan/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
										<button type='button' class='close' data-dismiss='alert'>
											<i class='icon-remove'></i>
										</button>

										<p>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Data laporan pelayanan berhasil dihapus...!
										</p>
									</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data laporan berhasil dihapus!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
		else
		{
			header('location:'.base_url().'index.php/panel');	
		}
	}

	public function deletePasangBaru()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$uuid_pasang_baru	 	= $this->uri->segment(3);
			$foto 					= $this->uri->segment(4);;
			$hapus 					= array('uuid_pasang_baru'=>$uuid_pasang_baru);
			$path 					= './upload/pasang_baru/';
      		@unlink($path.$foto);
			
			$this->web_app_model->deleteData('app_pasangBaru',$hapus);
			//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
			header('location:'.base_url().'index.php/panel/bg_pasangBaru?pasangBaru=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
										<button type='button' class='close' data-dismiss='alert'>
											<i class='icon-remove'></i>
										</button>

										<p>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Data permintaan pasang baru berhasil dihapus...!
										</p>
									</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data permintaan pasang baru berhasil dihapus!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
		else
		{
			header('location:'.base_url().'index.php/panel');	
		}
	}

	public function tambahPetugas()
	{
		$nama					= $this->input->post('nama');
		$wa						= $this->input->post('wa');

		$data = array(		
			'nama' 				=> $nama,
			'wa'				=> $wa,
			);
		
		$this->web_app_model->insertData($data,'app_petugasLap');
		header('location:'.base_url().'index.php/panel/bg_petugasLap/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Petugas berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data petugas berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function kirimTugas()
	{
		$petugas				= $this->input->post('petugas');
		//$pesan				= $this->input->post('pesan');

		$pic 					= $this->web_app_model->getWhereOneItem($petugas,'wa','app_petugasLap');

		$id_pelapor				= $this->input->post('id_pelapor');
		$uuid_complaint			= $this->input->post('uuid_complaint');
		$id_rpt					= $this->input->post('id_report');
		$st_plp					= $this->input->post('stts_pelapor');
		$nm_plp2				= $this->input->post('nama_pelapor');
		$nm_plp                 = str_replace("'"," ",$nm_plp2);
		$hp_plp					= $this->input->post('hp_pelapor');
		$em_plp					= $this->input->post('email_pelapor');
		$kg_com					= $this->input->post('kateg_complaint');
		$lat					= $this->input->post('lat');
		$lng					= $this->input->post('lng');
		$isi					= $this->input->post('isi');
		$attach					= $this->input->post('attach');

		$data = array(		
			'stts_complaint' 	=> 'Process',
			'pic_complaint' 	=> $petugas,
			'nm_pic_complaint' 	=> $pic['nama'],
			);

		$where = array(		
			'uuid_complaint' 	=> $uuid_complaint,
			);

		$this->web_app_model->updateDataWhere($where,$data,'app_complaint');
		header('location:'.base_url().'index.php/panel/bg_detailComplaint/'.$uuid_complaint.'');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													ID Report RPT-$id_rpt sedang di Prosess!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data petugas berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											     window.setTimeout(function(){ window.open('https://api.whatsapp.com/send?phone=".$petugas."&text=*[TIKET GANGGUAN: RPT-".$id_rpt."]*%0A%0AID Pelapor : *".$id_pelapor."*%0ANama : *".$nm_plp."*%0AStatus : ".$st_plp."%0AHP : ".$hp_plp."%0AEmail : ".$em_plp."%0AJenis Laporan : ".$kg_com."%0A*Deskripsi :* %0A".$isi."%0A%0ALocation : %0Ahttps://www.google.co.id/maps/search/".$lat.",".$lng."/%0A%0AAttachment :%0A".base_url()."upload/complaint/".$attach."','_blank'); } ,3000);
											    </script>
											    ");
	}

	public function kirimTugas_pasangBaru()
	{
		$petugas				= $this->input->post('petugas');
		$id_desa_pemohon		= $this->input->post('id_desa_pemohon');
		$id_kec_pemohon			= $this->input->post('id_kec_pemohon');

		$pic 					= $this->web_app_model->getWhereOneItem($petugas,'wa','app_petugasLap');
		$desa 					= $this->web_app_model->getWhereOneItem($id_desa_pemohon,'DesaID','tbl_desa');
		$kecamatan 				= $this->web_app_model->getWhereOneItem($id_kec_pemohon,'KecamatanID','tbl_kecamatan');

		$uuid_pasang_baru		= $this->input->post('uuid_pasang_baru');
		$id_reg					= $this->input->post('id_reg');
		$ktp					= $this->input->post('ktp_pemohon');
		$nm						= $this->input->post('nama_pemohon');
		$hp						= $this->input->post('hp_pemohon');
		$ds						= $desa['Desa'];
		$kec					= $kecamatan['Kecamatan'];
		$almt					= $this->input->post('alamat_pemohon');
		$lat					= $this->input->post('lat_pemohon');
		$lng					= $this->input->post('lng_pemohon');
		$attach					= $this->input->post('img_ktp');

		$data = array(		
			'stts_pasang_baru' 	=> 'Process',
			'pic_pasang_baru' 	=> $petugas,
			'nm_pic_pasang_baru'=> $pic['nama'],
			);

		$where = array(		
			'uuid_pasang_baru' 	=> $uuid_pasang_baru,
			);

		$this->web_app_model->updateDataWhere($where,$data,'app_pasangBaru');
		header('location:'.base_url().'index.php/panel/bg_pasangBaru?pasangBaru=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													ID Registrasi REG-$id_reg sedang di Prosess!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data petugas berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											     window.setTimeout(function(){ window.open('https://api.whatsapp.com/send?phone=".$petugas."&text=*[TIKET SURVEY PASANG BARU: REG-".$id_reg."]*%0A%0AID KTP : ".$ktp."%0ANama : *".$nm."*%0AHP : ".$hp."%0ADesa : ".$ds."%0AKec. : ".$kec."%0A*Alamat Detail* : ".$almt."%0A%0ALocation : %0Ahttps://www.google.co.id/maps/search/".$lat.",".$lng."/%0A%0AFoto KTP :%0A".base_url()."upload/pasang_baru/".$attach." %0A%0ANote:%0AHarap konfirmasi/lapor apabila status pasang baru diterima dan lanjut ke proses instalasi atau di tolak karena alasan tertentu. Dan konfirmasi/lapor pula apabila status instalasi sudah selesai terpasang. Selamat Bekerja :) ','_blank'); } ,3000);
											    </script>
											    ");
	}

	public function updateInfoGangguan()
	{
		date_default_timezone_set('Asia/Jakarta');
		$gangguan_updated_at	= date('Y-m-d H:i:s');
		$judul_gangguan			= $this->input->post('judul_gangguan');
		$detail_gangguan		= $this->input->post('detail_gangguan');
		$stts_gangguan			= $this->input->post('stts_gangguan');
		$id_gangguan			= "1";

		$data = array(		
			'gangguan_updated_at' 	=> $gangguan_updated_at,
			'judul_gangguan' 		=> $judul_gangguan,
			'detail_gangguan' 		=> $detail_gangguan,
			'stts_gangguan' 		=> $stts_gangguan,
			);

		$where = array(		
			'id_gangguan' 			=> $id_gangguan,
			);

		$this->web_app_model->updateDataWhere($where,$data,'app_gangguan');
		
		header('location:'.base_url().'index.php/panel/bg_pengumuman/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Info gangguan berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Updated!!',
											                text:  'Info gangguan berhasil diupdate',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function closeComplaint()
	{
		$uuid_complaint				= $this->uri->segment(3);
		$completion					= date('Y-m-d H:i:s');

		$data = array(		
			'stts_complaint' 	=> 'Clear',
			'completion' 		=> $completion,
			);

		$where = array(		
			'uuid_complaint' 	=> $uuid_complaint,
			);

		$this->web_app_model->updateDataWhere($where,$data,'app_complaint');

		header('location:'.base_url().'index.php/panel/bg_detailComplaint/'.$uuid_complaint.'');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Tiket laporan gangguan selesai!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Good Job!!',
											                text:  'Data gangguan clear, dan sudah di tutup!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											 
											    </script>
											    ");
	}

	public function skipKirimTugas()
	{
		$uuid_complaint			= $this->uri->segment(3);

		$data = array(		
			'stts_complaint' 	=> 'Process',
			);

		$where = array(		
			'uuid_complaint' 	=> $uuid_complaint,
			);

		$this->web_app_model->updateDataWhere($where,$data,'app_complaint');
		//header('location:https://api.whatsapp.com/send?phone='.$petugas.'&text=*[Tiket Gangguan: RPT-'.$id_rpt.']*%0A%0ANama : *'.$nm_plp.'*%0AStatus : '.$st_plp.'%0AHP : '.$hp_plp.'%0AEmail : '.$em_plpm.'%0AJenis Laporan : '.$kg_com.'%0ADeskripsi : %0A'.$isi.'%0A%0ATitik Lokasi: https://www.google.co.id/maps/search/'.$lat.','.$lng.'/%0A');
		header('location:'.base_url().'index.php/panel/bg_detailComplaint/'.$uuid_complaint.'');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Gangguan sedang di Prosess!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Tiket Gangguan sedang dalam process!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function nextInstalasi()
	{
		$uuid_pasang_baru			= $this->uri->segment(3);

		$data = array(		
			'stts_pasang_baru' 		=> 'Instalasi',
			);

		$where = array(		
			'uuid_pasang_baru' 	=> $uuid_pasang_baru,
			);

		$this->web_app_model->updateDataWhere($where,$data,'app_pasangBaru');
		header('location:'.base_url().'index.php/panel/bg_pasangBaru?pasangBaru=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Status Pasang baru lanjut ke Tahap Intalasi!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Tiket Pasang Baru lanjut ke tahap Intalasi!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function tolakPasangBaru()
	{
		$uuid_pasang_baru			= $this->uri->segment(3);

		$data = array(		
			'stts_pasang_baru' 		=> 'Reject',
			);

		$where = array(		
			'uuid_pasang_baru' 	=> $uuid_pasang_baru,
			);

		$this->web_app_model->updateDataWhere($where,$data,'app_pasangBaru');
		header('location:'.base_url().'index.php/panel/bg_pasangBaru?pasangBaru=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Status Pasang baru ditolak!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Tiket Pasang Baru sudah ditolak!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function selesaiPasangBaru()
	{
		$uuid_pasang_baru				= $this->uri->segment(3);
		$finished_at_pasang_baru		= date('Y-m-d H:i:s');

		$data = array(		
			'stts_pasang_baru' 			=> 'Clear',
			'finished_at_pasang_baru' 	=> $finished_at_pasang_baru,
			);

		$where = array(		
			'uuid_pasang_baru' 	=> $uuid_pasang_baru,
			);

		$this->web_app_model->updateDataWhere($where,$data,'app_pasangBaru');
		header('location:'.base_url().'index.php/panel/bg_pasangBaru?pasangBaru=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Status Pasang baru selesai!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Tiket Pasang Baru Selesai!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapusPetugas()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			$id 		= $this->uri->segment(3);
			$hapus 		= array('id_petugasLap'=>$id);
			
			$this->web_app_model->deleteData('app_petugasLap',$hapus);
			//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
			header('location:'.base_url().'index.php/panel/bg_petugasLap/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
										<button type='button' class='close' data-dismiss='alert'>
											<i class='icon-remove'></i>
										</button>

										<p>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Data petugas berhasil dihapus...!
										</p>
									</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data petugas berhasil dihapus!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
		else
		{
			header('location:'.base_url().'index.php/panel');	
		}
	}

	public function addArtikel()
	{
		date_default_timezone_set('Asia/Jakarta');
		$uuid					= $this->get_id();
		$author 				= $this->session->userdata('nama');
		$judul					= $this->input->post('judul');
		$konten					= $this->input->post('konten');
		$kategori_artikel		= $this->input->post('kategori_artikel');
		$artikel_updated_at		= date('Y-m-d H:i:s');

		// get foto
		  $config['upload_path'] 	= './upload/artikel';
		  $config['allowed_types'] 	= 'jpg|png|jpeg';
		  $config['max_size'] 		= '5000';  //5MB max
		 // $config['max_width'] 	= '4480'; // pixel
		  //$config['max_height'] 	= '4480'; // pixel
		  $config['overwrite']		= true;
		  $config['file_name'] 		= $uuid;

     	  $this->load->library('upload', $config);
		
		if(!empty($_FILES['img']['name'])) 
		{
	        if ( $this->upload->do_upload('img') ) {
	            $foto = $this->upload->data();		
				$data = array(					
					'uuid' 						=> $uuid,
					'judul'						=> $judul,
					'author'					=> $author,
					'konten'					=> $konten,
					'kategori_artikel'			=> $kategori_artikel,
					'artikel_updated_at' 		=> $artikel_updated_at,
					'img' 						=> $foto['file_name'],	
					);
				
				$this->web_app_model->insertData($data,'app_artikel');
				header('location:'.base_url().'index.php/panel/bg_artikel/');
				$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
														<button type='button' class='close' data-dismiss='alert'>
															<i class='icon-remove'></i>
														</button>
				
														<p>
															<strong>
																<i class='icon-ok'></i>
																Success!
															</strong>
													Data Artikel berhasil ditambahkan!
												</p>
											</div>");

				$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data artikel berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
			}
			else 
			{
           		header('location:'.base_url().'index.php/panel/bg_artikel');
				$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Ups!
													</strong>
													Gagal Upload
												</p>
											</div>");

				$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Gagal!!',
											                text:  'Data artikel gagal ditambahkan!',
											                type: 'warning',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	    	}
	    }
		else 
		{
	      header('location:'.base_url().'index.php/panel/bg_artikel');
				$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Ups!
													</strong>
													File Tidak Masuk !!
												</p>
											</div>");

				$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Cover image kosong!!',
											                text:  'Data artikel gagal ditambahkan!',
											                type: 'warning',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	    }
	}

	public function updateArtikel()
	{
		date_default_timezone_set('Asia/Jakarta');
		$uuid					= $this->input->post('uuid');
		$author 				= $this->session->userdata('nama');
		$judul					= $this->input->post('judul');
		$konten					= $this->input->post('konten');
		$kategori_artikel		= $this->input->post('kategori_artikel');
		$artikel_updated_at		= date('Y-m-d H:i:s');

		$data = array(					
			'judul'						=> $judul,
			'author'					=> $author,
			'konten'					=> $konten,
			'kategori_artikel'			=> $kategori_artikel,
			'artikel_updated_at' 		=> $artikel_updated_at,
			);

		$where = array(
			'uuid' 						=> $uuid,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'app_artikel');
		header('location:'.base_url().'index.php/panel/bg_artikel/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success!
													</strong>
											Data Artikel berhasil diupdate!
										</p>
									</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Success!!',
									                text:  'Data artikel berhasil diupdate!',
									                type: 'success',
									                timer: 3000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>
									    ");
	}


	public function updateGambarArtikel()
	{
		date_default_timezone_set('Asia/Jakarta');
		$uuid					= $this->input->post('uuid');
		$artikel_updated_at		= date('Y-m-d H:i:s');

		// get foto
		  $config['upload_path'] 	= './upload/artikel';
		  $config['allowed_types'] 	= 'jpg|png|jpeg';
		  $config['max_size'] 		= '5000';  //5MB max
		 // $config['max_width'] 	= '4480'; // pixel
		  //$config['max_height'] 	= '4480'; // pixel
		  $config['overwrite']		= true;
		  $config['file_name'] 		= $uuid;

     	  $this->load->library('upload', $config);
		
		if(!empty($_FILES['img']['name'])) 
		{
	        if ( $this->upload->do_upload('img') ) {
	            $foto = $this->upload->data();		
				$data = array(					
					'artikel_updated_at' 		=> $artikel_updated_at,
					'img' 						=> $foto['file_name'],	
					);

				$where = array(
					'uuid' 						=> $uuid,
					);
				
				$this->web_app_model->updateDataWhere($where,$data,'app_artikel');
				header('location:'.base_url().'index.php/panel/bg_artikel/');
				$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
														<button type='button' class='close' data-dismiss='alert'>
															<i class='icon-remove'></i>
														</button>
				
														<p>
															<strong>
																<i class='icon-ok'></i>
																Success!
															</strong>
													Sampul Artikel berhasil diupdate!
												</p>
											</div>");

				$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Sampul artikel berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
			}
			else 
			{
           		header('location:'.base_url().'index.php/panel/bg_editArtikel/'.$uuid.'');
				$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Ups!
													</strong>
													Gagal Upload
												</p>
											</div>");

				$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Gagal!!',
											                text:  'Sampul artikel gagal diupdate!',
											                type: 'warning',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	    	}
	    }
		else 
		{
	      header('location:'.base_url().'index.php/panel/bg_editArtikel/'.$uuid.'');
				$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Ups!
													</strong>
													File Tidak Masuk !!
												</p>
											</div>");

				$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Cover image kosong!!',
											                text:  'sampul artikel gagal diupdate!',
											                type: 'warning',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	    }
	}

	public function deleteArtikel()
	{
		$cek  = $this->session->userdata('logged_in');
		if(!empty($cek))
		{
			$uuid	 				= $this->uri->segment(3);
			$foto 					= $this->uri->segment(4);;
			$hapus 					= array('uuid'=>$uuid);
			$path 					= './upload/artikel/';
      		@unlink($path.$foto);
			
			$this->web_app_model->deleteData('app_artikel',$hapus);
			//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
			header('location:'.base_url().'index.php/panel/bg_artikel/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
										<button type='button' class='close' data-dismiss='alert'>
											<i class='icon-remove'></i>
										</button>

										<p>
											<strong>
												<i class='icon-ok'></i>
												Okay!
											</strong>
											Data artikel berhasil dihapus...!
										</p>
									</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data artikel berhasil dihapus!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
		else
		{
			header('location:'.base_url().'index.php/panel');	
		}
	}
}