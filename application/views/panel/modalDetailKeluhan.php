<?php 
    foreach($data_laporan->result_array() as $d){
?>
<div id="modalDetailKeluhan<?php echo $d['uuid_complaint'] ?>" class="modal fade" tabindex="-1" data-width="660" style="display: none;">
    <form role="form" action="#" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Deskripsi Keluhan <?php echo $d['nama_pelapor'] ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Detail Keluhan Pelapor:</label>
                    <p>
                        <textarea
                            class="form-control"
                            disabled
                            style="resize:none;width:100%;height:250px;"><?php echo $d['isi_laporan'] ?></textarea>
                    </p>
                   
                </div>               
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
        </div>
    </form>
</div>
<?php } ?>