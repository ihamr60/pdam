<div id="modalPilihPetugas" class="modal fade" tabindex="-1" data-width="360" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/panel/kirimTugas" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Pilih Petugas Lapangan</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Pilih petugas lapangan:</label>
                    <p>
                        <select
                            class="form-control"
                            name="petugas">
                            <option value="">Plese Select</option>
                            <?php foreach($data_petugas->result_array() as $d){ ?>
                                <option value="<?php echo $d['wa'] ?>"><?php echo $d['nama'] ?></option>
                            <?php } ?>
                        </select>
                    </p>
                   
                </div>
                
                    <input
                        name="id_pelapor"
                        value="<?php echo $dataReport['id_pelapor'] ?>"
                        type="hidden"
                    >
                    <input
                        name="uuid_complaint"
                        value="<?php echo $dataReport['uuid_complaint'] ?>"
                        type="hidden"
                    >
                    <input
                        name="id_report"
                        value="<?php echo $dataReport['id_report'] ?>"
                        type="hidden"
                    >
                    <input
                        name="stts_pelapor"
                        value="<?php echo $dataReport['stts_pelapor'] ?>"
                        type="hidden"
                    >
                     <input
                        name="nama_pelapor"
                        value="<?php echo $dataReport['nama_pelapor'] ?>"
                        type="hidden"
                    >
                     <input
                        name="hp_pelapor"
                        value="<?php echo $dataReport['hp_pelapor'] ?>"
                        type="hidden"
                    >
                     <input
                        name="email_pelapor"
                        value="<?php echo $dataReport['email_pelapor'] ?>"
                        type="hidden"
                    >
                     <input
                        name="kateg_complaint"
                        value="<?php echo $dataReport['kateg_complaint'] ?>"
                        type="hidden"
                    >
                     <input
                        name="lat"
                        value="<?php echo $dataReport['lat_complaint'] ?>"
                        type="hidden"
                    >
                     <input
                        name="lng"
                        value="<?php echo $dataReport['lng_complaint'] ?>"
                        type="hidden"
                    >
                    <input
                        name="isi"
                        value="<?php echo $dataReport['isi_laporan'] ?>"
                        type="hidden"
                    >
                    <input
                        name="attach"
                        value="<?php echo $dataReport['attach'] ?>"
                        type="hidden"
                    >
           
                
            </div>
        </div>
        <div class="modal-footer">
            <a href="<?php echo base_url() ?>index.php/panel/skipKirimTugas/<?php echo $dataReport['uuid_complaint'] ?>" class="btn btn-light-grey">
                Skip Petugas
            </a>
            <button type="submit" class="btn btn-blue">
                Tugaskan & Process
            </button>
        </div>
    </form>
</div>