
<div id="modalAttach<?php echo $dataReport['uuid_complaint'] ?>" class="modal fade" tabindex="-1" data-width="660" style="display: none;">
    <form role="form" action="#" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Lampiran a.n saudara/i <?php echo $dataReport['nama_pelapor'] ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo base_url() ?>upload/complaint/<?php echo $dataReport['attach'] ?>" style=" border-radius: 10px; width: 620px; height: 650px">   
                </div>               
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
        </div>
    </form>
</div>
