<div id="modalTambahPetugas" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/panel/tambahPetugas" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Tambahkan Petugas Lapangan</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Nama Petugas:</label>
                    <p>
                        <input
                            type="text"
                            name="nama"
                            class="form-control"
                            placeholder="Ex: Ilham Ramadhan"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Kontak WA Petugas:</label>
                    <p>
                        <input
                            type="number"
                            name="wa"
                            class="form-control"
                            placeholder="Gunakan 62 diawal | Ex: 6285361885100"
                            required>
                    </p>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>