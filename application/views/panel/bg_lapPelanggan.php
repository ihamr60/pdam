<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]--><head>
    <title>Panel E-PDAM Langsa</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

   
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
    <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="clip-home-3"></i>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Laporan Pelanggan
                            </li>
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> REKAP LAPORAN PELANGGAN PDAM 
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                                    <a data-original-title="RESIZE FULL" data-content="Klik icon tersebut untuk tampilan full table" data-placement="top" data-trigger="hover" id="test" class="btn btn-xs btn-link panel-expand popovers" href="#"> <i class="fa fa-laptop"></i> </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <form method="post" action="<?php echo base_url();?>index.php/panel/bg_lapPelanggan_filter" target="_blank">
                                    <div class="col-md-6">
                                        <label>From date:</label>
                                        <p>
                                            <input
                                                type="date"
                                                name="from_date"
                                                class="form-control"
                                                required>
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <label>To date:</label>
                                        <p>
                                            <input
                                                type="date"
                                                name="to_date"
                                                class="form-control"
                                                required>
                                        </p>
                                    </div>
                                    <div class="col-md-12 space20">
                                        <button class="btn btn-orange">
                                            Filter Data <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                    </form>
                                </div> 
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="10" class="col-to-export">#</th>
                                                <th width="10" class="col-to-export center">ID RPT</th>
                                                <th width="50" class="col-to-export">ID PEL</th>
                                                <th width="140" class="col-to-export">NAMA PELANGGAN</th>
                                                <th width="100" class="col-to-export center">NO. HP</th>
                                                <th class="col-to-export center">JENIS LAPORAN</th>
                                                <th class="col-to-export">STATUS</th>
                                                <th>KELUHAN PELANGGAN</th>
                                                <th class="col-to-export">CREATED</th>
                                                <th class="col-to-export center">PIC</th>
                                                <th class="col-to-export center">SELESAI</th>
                                                <th class="center">ACT</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                function limit_words($string, $word_limit){
                                                    $words = explode(" ",$string);
                                                    return implode(" ",array_splice($words,0,$word_limit));
                                                }
                                                foreach ($data_laporan->result_array() as $d)
                                                {
                                            ?>

                                                <?php 
                                                    if($d['stts_complaint']=="Pending")
                                                    {
                                                ?>
                                                    <tr style="background-color: #f99191;">
                                                        <td align="center"><?php echo $d['id_report'] ?></td>
                                                        <td align="center">RPT-<?php echo $d['id_report'] ?></td>
                                                        <td align="right"><b><?php echo $d['id_pelapor'] ?></b></td>
                                                        <td><b><?php echo $d['nama_pelapor'] ?></b></td>
                                                        <td class="center"><?php echo $d['hp_pelapor'] ?></td>
                                                        <td class="center"><b><?php echo $d['kateg_complaint'] ?></b></td>
                                                        <td><?php 
                                                                if($d['stts_complaint']=="Pending")
                                                                {
                                                                    echo "<span class='label label-orange'>PENDING</span>";
                                                                }
                                                                else if ($d['stts_complaint']=="Process") {
                                                                    echo "<span class='label label-teal'>PROCESS</span>";
                                                                }
                                                                else if ($d['stts_complaint']=="Clear") {
                                                                    echo "<span class='label label-green'> SELESAI </span>";
                                                                }
                                                            ?></td>
                                                        <td><?php echo limit_words($d['isi_laporan'],9) ?> <a data-toggle="modal" href="#" data-target="#modalDetailKeluhan<?php echo $d['uuid_complaint'] ?>">Selengkapnya..</a></td>
                                                        <td><?php echo date('d/m/Y', strtotime($d['complaint_updated_at'])) ?></td>
                                                        <td class="center" style="text-transform: uppercase;"><b><?php 
                                                            if($d['pic_complaint']!="")
                                                            {
                                                                echo $d['nm_pic_complaint'];
                                                            }
                                                            else
                                                            {
                                                                echo "-";
                                                            }
                                                         ?></b></td>
                                                         <td class="center"><b>
                                                             <?php 
                                                            if($d['completion']!="")
                                                            {
                                                                echo date('d/m/Y', strtotime($d['completion']));
                                                            }
                                                            else
                                                            {
                                                                echo "<span class='label label-danger'> Belum Selesai </span>";
                                                            }
                                                         ?></b>
                                                         </td>
                                                        <td align="center">
                                                            <a href="<?php echo base_url();?>index.php/panel/bg_detailComplaint/<?php echo $d['uuid_complaint'] ?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Follow Up"><i class="fa fa-arrow-up"></i></a>

                                                            <a href="<?php echo base_url();?>index.php/panel/deleteComplaint/<?php echo $d['uuid_complaint'] ?>/<?php echo $d['attach'] ?>" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan menghapus data laporan ini?')"><i class="fa fa-trash"></i></a></td>
                                                        
                                                    </tr>
                                            <?php   } ?>
                                                <?php 
                                                    if($d['stts_complaint']=="Clear")
                                                    {
                                                ?>
                                                    <tr style="background-color: #65d462;">
                                                        <td align="center"><?php echo $d['id_report'] ?></td>
                                                        <td align="center">RPT-<?php echo $d['id_report'] ?></td>
                                                        <td align="right"><b><?php echo $d['id_pelapor'] ?></b></td>
                                                        <td><b><?php echo $d['nama_pelapor'] ?></b></td>
                                                        <td class="center"><?php echo $d['hp_pelapor'] ?></td>
                                                        <td class="center"><b><?php echo $d['kateg_complaint'] ?></b></td>
                                                        <td><?php 
                                                                if($d['stts_complaint']=="Pending")
                                                                {
                                                                    echo "<span class='label label-orange'>PENDING</span>";
                                                                }
                                                                else if ($d['stts_complaint']=="Process") {
                                                                    echo "<span class='label label-teal'>PROCESS</span>";
                                                                }
                                                                else if ($d['stts_complaint']=="Clear") {
                                                                    echo "<span class='label label-green'> SELESAI </span>";
                                                                }
                                                            ?></td>
                                                        <td><?php echo limit_words($d['isi_laporan'],9) ?> <a data-toggle="modal" href="#" data-target="#modalDetailKeluhan<?php echo $d['uuid_complaint'] ?>">Selengkapnya..</a></td>
                                                        <td><?php echo date('d/m/Y', strtotime($d['complaint_updated_at'])) ?></td>
                                                        <td class="center" style="text-transform: uppercase;"><b><?php 
                                                            if($d['pic_complaint']!="")
                                                            {
                                                                echo $d['nm_pic_complaint'];
                                                            }
                                                            else
                                                            {
                                                                echo "-";
                                                            }
                                                         ?></b></td>
                                                         <td class="center"><b>
                                                             <?php 
                                                            if($d['completion']!="")
                                                            {
                                                                echo date('d/m/Y', strtotime($d['completion']));
                                                            }
                                                            else
                                                            {
                                                                echo "-";
                                                            }
                                                         ?></b>
                                                         </td>
                                                        <td align="center">
                                                            <a href="<?php echo base_url();?>index.php/panel/bg_detailComplaint/<?php echo $d['uuid_complaint'] ?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Follow Up"><i class="fa fa-arrow-up"></i></a>

                                                            <a href="<?php echo base_url();?>index.php/panel/deleteComplaint/<?php echo $d['uuid_complaint'] ?>/<?php echo $d['attach'] ?>" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan menghapus data laporan ini?')"><i class="fa fa-trash"></i></a></td>
                                                        
                                                    </tr>
                                            <?php   } ?>
                                            <?php 
                                                    if($d['stts_complaint']=="Process")
                                                    {
                                                ?>
                                                    <tr style="background-color: #93e1f7;">
                                                        <td align="center"><?php echo $d['id_report'] ?></td>
                                                        <td align="center">RPT-<?php echo $d['id_report'] ?></td>
                                                        <td align="right"><b><?php echo $d['id_pelapor'] ?></b></td>
                                                        <td><b><?php echo $d['nama_pelapor'] ?></b></td>
                                                        <td class="center"><?php echo $d['hp_pelapor'] ?></td>
                                                        <td class="center"><b><?php echo $d['kateg_complaint'] ?></b></td>
                                                        <td><?php 
                                                                if($d['stts_complaint']=="Pending")
                                                                {
                                                                    echo "<span class='label label-orange'>PENDING</span>";
                                                                }
                                                                else if ($d['stts_complaint']=="Process") {
                                                                    echo "<span class='label label-teal'>PROCESS</span>";
                                                                }
                                                                else if ($d['stts_complaint']=="Clear") {
                                                                    echo "<span class='label label-green'> SELESAI </span>";
                                                                }
                                                            ?></td>
                                                        <td><?php echo limit_words($d['isi_laporan'],9) ?> <a data-toggle="modal" href="#" data-target="#modalDetailKeluhan<?php echo $d['uuid_complaint'] ?>">Selengkapnya..</a></td>
                                                        <td><?php echo date('d/m/Y', strtotime($d['complaint_updated_at'])) ?></td>
                                                        <td class="center" style="text-transform: uppercase;"><b><?php 
                                                            if($d['pic_complaint']!="")
                                                            {
                                                                echo $d['nm_pic_complaint'];
                                                            }
                                                            else
                                                            {
                                                                echo "-";
                                                            }
                                                         ?></b></td>
                                                         <td class="center"><b>
                                                             <?php 
                                                            if($d['completion']!="")
                                                            {
                                                                echo date('d/m/Y', strtotime($d['completion']));
                                                            }
                                                            else
                                                            {
                                                                echo "<span class='label label-danger'> Belum Selesai </span>";
                                                            }
                                                         ?></b>
                                                         </td>
                                                        <td align="center">
                                                            <a href="<?php echo base_url();?>index.php/panel/bg_detailComplaint/<?php echo $d['uuid_complaint'] ?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Follow Up"><i class="fa fa-arrow-up"></i></a>

                                                            <a href="<?php echo base_url();?>index.php/panel/deleteComplaint/<?php echo $d['uuid_complaint'] ?>/<?php echo $d['attach'] ?>" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan menghapus data laporan ini?')"><i class="fa fa-trash"></i></a></td>
                                                        
                                                    </tr>
                                            <?php   } ?>

                                        <?php   } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> PRINT OUT SURAT LAPORAN RESMI
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                                    <a data-original-title="RESIZE FULL" data-content="Klik icon tersebut untuk tampilan full table" data-placement="top" data-trigger="hover" id="test" class="btn btn-xs btn-link panel-expand popovers" href="#"> <i class="fa fa-laptop"></i> </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <form method="post" action="<?php echo base_url();?>index.php/panel/bg_lapPelanggan_report" target="_blank">
                                    <div class="col-md-6">
                                        <label>From date:</label>
                                        <p>
                                            <input
                                                type="date"
                                                name="from_date"
                                                class="form-control"
                                                required>
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <label>To date:</label>
                                        <p>
                                            <input
                                                type="date"
                                                name="to_date"
                                                class="form-control"
                                                required>
                                        </p>
                                    </div>
                                    <div class="col-md-12 space20">
                                        <button class="btn btn-orange">
                                            Print Out <i class="clip-file-2"></i>
                                        </button>
                                    </div>
                                    </form>
                                </div> 
                                <div class="table-responsive">
                                    
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php echo $modalDetailKeluhan; ?>
			<!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php $this->load->view('versi'); ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    
    
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

     <?php echo $this->session->flashdata('info2'); ?>

    <script>
        jQuery(document).ready(function() {
            Main.init(); 
			UIElements.init();
            TableExport.init();
			UIModals.init();
            UIButtons.init();
        });
    </script>

</body>

</html>