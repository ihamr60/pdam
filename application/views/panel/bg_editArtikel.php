<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>E-PDAM Kota Langsa</title>
    <link rel="shortcut icon" href="../admin_cabang/favicon.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url(); ?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/assets/plugin/bootstrap-timepicker.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="clip-home-3"></i>
                                <a href="<?php echo base_url(); ?>index.php/panel">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url() ?>index.php/panel/bg_artikel">
                                    Data Artikel
                                </a>
                            </li>
                            <li class="active">
                                Edit Artikel
                            </li>
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><b>UPDATE ARTIKEL E-PDAM KOTA LANGSA</b>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal"> <i class="fa fa-wrench"></i> </a>
                        <a class="btn btn-xs btn-link panel-refresh" href="#"> <i class="fa fa-refresh"></i> </a>
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a>
                        <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                    </div>
                </div>
              
                <div class="panel-body">  
                    <form role="form" action="<?php echo base_url();?>index.php/panel/updateArtikel/" method="post">
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="row">
                        
                            <div class="col-md-12">
                                <br/>
                                <label><b>JUDUL ARTIKEL :</b></label>
                                <p>
                                    <input type="hidden" value="<?php echo $data_artikel['uuid']?>" name="uuid">
                                    <input 
                                    required 
                                    value="<?php echo $data_artikel['judul'] ?>"
                                    name="judul" 
                                    class="form-control" 
                                    type="text">
                                </p>     
                            </div>
                            <div  class="radio col-md-6">
                                <?php if($data_artikel['kategori_artikel']=="Story")
                                { 
                                    echo "
                                    <label>
                                        <input 
                                            type='radio' 
                                            value='Story' 
                                            checked='checked' 
                                            class='green' 
                                            name='kategori_artikel'
                                            required>
                                        Story
                                    </label>
                                    <label>
                                        <input 
                                            type='radio' 
                                            value='Tips' 
                                            class='purple' 
                                            name='kategori_artikel'
                                            required>
                                        Tips
                                    </label>";
                                }
                                else if($data_artikel['kategori_artikel']=="Tips")
                                { 
                                    echo "
                                    <label>
                                        <input 
                                            type='radio' 
                                            value='Story' 
                                            class='green' 
                                            name='kategori_artikel'
                                            required>
                                        Story
                                    </label>
                                    <label>
                                        <input 
                                            type='radio' 
                                            value='Tips' 
                                            checked='checked' 
                                            class='purple' 
                                            name='kategori_artikel'
                                            required>
                                        Tips
                                    </label>";
                                }

                                ?>
                                
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group"><br/>
                                    <label class="control-label">
                                       <b> ISI / KONTEN ARTIKEL </b>
                                    </label>
                                    <textarea name="konten" class="ckeditor">
                                        <?php echo $data_artikel['konten'] ?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group" style="padding-left:1050px;">
                                    <a href="<?php echo base_url(); ?>index.php/panel/bg_artikel" type="button" class="btn btn-light-grey">
                                        Kembali
                                    </a>
                                    <button type="submit" class="btn btn-blue">
                                        Update Artikel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
              </div>  




              <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><b>UPDATE GAMBAR ARTIKEL (COVER)</b>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal"> <i class="fa fa-wrench"></i> </a>
                        <a class="btn btn-xs btn-link panel-refresh" href="#"> <i class="fa fa-refresh"></i> </a>
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a>
                        <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                    </div>
                </div>
              
                <div class="panel-body">  
                    <form role="form" action="<?php echo base_url() ?>index.php/panel/updateGambarArtikel" method="post" enctype="multipart/form-data">
                        <div class="row">                            
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label>
                                        Feature Image / Cover Image
                                    </label>
                                    <div id="kv-avatar-errors" class="center-block" style="display:none"></div>
                                    <div class="kv-avatar ">
                                        <input type="hidden" value="<?php echo $data_artikel['uuid']?>" name="uuid">
                                        <input id="avatar" name="img" type="file" class="file-loading">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <img style="border-radius: 10px;" width="220px" height="200px" src="<?php echo base_url() ?>upload/artikel/<?php echo $data_artikel['img'] ?>">
                                </div>
                            </div>

                            <div class="form-group" style="padding-left:1050px;">
                                <a href="<?php echo base_url(); ?>index.php/panel/bg_artikel" type="button" class="btn btn-light-grey">
                                    Kembali
                                </a>
                                <button type="submit" class="btn btn-blue">
                                    Update Gambar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
              </div>  
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- start: BOOTSTRAP EXTENDED MODALS -->
            <?php //echo $modalTambahAdminPusat; ?>
            <!-- END : BOOTSTRAP EXTEND MODALS -->  
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php $this->load->view('versi'); ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url(); ?>vendor/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/autosize/dist/autosize.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/summernote/dist/summernote.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/assets/js/min/form-elements.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <?php echo $this->session->flashdata('info2'); ?>
    <script>
        jQuery(document).ready(function() {
            Main.init();
            FormElements.init();
        });
    </script>

</body>

</html>