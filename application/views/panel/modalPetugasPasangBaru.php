<?php 
                foreach ($data_pasangBaru->result_array() as $d)
                {
            ?>
            <div id="modalPilihPetugas<?php echo $d['uuid_pasang_baru'] ?>" class="modal fade" tabindex="-1" data-width="360" style="display: none;">
                <form role="form" action="<?php echo base_url();?>index.php/panel/kirimTugas_pasangBaru" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Pilih Petugas Lapangan</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Pilih petugas lapangan:</label>
                                <p>
                                    <select
                                        class="form-control"
                                        name="petugas">
                                        <option value="">Plese Select</option>
                                        <?php foreach($data_petugas->result_array() as $e){ ?>
                                            <option value="<?php echo $e['wa'] ?>"><?php echo $e['nama'] ?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                               
                            </div>
                            
                                <input
                                    name="uuid_pasang_baru"
                                    value="<?php echo $d['uuid_pasang_baru'] ?>"
                                    type="hidden"
                                >
                                <input
                                    name="id_reg"
                                    value="<?php echo $d['id_reg'] ?>"
                                    type="hidden"
                                >
                                <input
                                    name="ktp_pemohon"
                                    value="<?php echo $d['ktp_pemohon'] ?>"
                                    type="hidden"
                                >
                                 <input
                                    name="nama_pemohon"
                                    value="<?php echo $d['nama_pemohon'] ?>"
                                    type="hidden"
                                >
                                 <input
                                    name="hp_pemohon"
                                    value="<?php echo $d['hp_pemohon'] ?>"
                                    type="hidden"
                                >
                                 <input
                                    name="alamat_pemohon"
                                    value="<?php echo $d['alamat_pemohon'] ?>"
                                    type="hidden"
                                >
                                 <input
                                    name="id_desa_pemohon"
                                    value="<?php echo $d['id_desa_pemohon'] ?>"
                                    type="hidden"
                                >
                                 <input
                                    name="id_kec_pemohon"
                                    value="<?php echo $d['id_kec_pemohon'] ?>"
                                    type="hidden"
                                >
                                 <input
                                    name="lat_pemohon"
                                    value="<?php echo $d['lat_pemohon'] ?>"
                                    type="hidden"
                                >
                                <input
                                    name="lng_pemohon"
                                    value="<?php echo $d['lng_pemohon'] ?>"
                                    type="hidden"
                                >
                                <input
                                    name="img_ktp"
                                    value="<?php echo $d['img_ktp'] ?>"
                                    type="hidden"
                                >
                       
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-blue">
                            Process
                        </button>
                    </div>
                </form>
            </div>
            <?php } ?>