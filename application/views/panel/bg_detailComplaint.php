<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]--><head>
    <title>Panel E-PDAM Langsa</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <style type="text/css">
    .uppercase{
    text-transform: uppercase;
    }
    </style>

    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />

    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />   
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" /> 
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

   
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
    <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="clip-home-3"></i>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Laporan Pelanggan
                            </li>
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Update selalu setiap ID Report ini apabila sudah selesai dengan kembali lagi ke halaman ini
                        </div>
                       <table id="user" class="table table-bordered table-striped" style="clear: both">
                            <tbody>
                                <tr>
                                    <td align="center"><b>FIELD LAPORAN</b></td>
                                    <td align="center"><b>IDENTITAS LAPORAN</b></td>
                                </tr>
                                <tr>
                                    <td align="right" class="column-left">ID REPORT</td>
                                    <td class="column-right">
                                        RPT-<?php echo $dataReport['id_report'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">ID PELAPOR</td>
                                    <td>
                                        <?php echo $dataReport['id_pelapor'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">STATUS PELAPOR</td>
                                    <td class="uppercase">
                                        <b><?php echo $dataReport['stts_pelapor'] ?></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">NAMA LENGKAP</td>
                                    <td class="uppercase">
                                        <?php echo $dataReport['nama_pelapor'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">NO.HP</td>
                                    <td>
                                        <?php echo $dataReport['hp_pelapor'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">E-MAIL</td>
                                    <td class="">
                                        <?php echo $dataReport['email_pelapor'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">JENIS KELUHAN / LAPORAN</td>
                                    <td class="uppercase">
                                        <?php echo $dataReport['kateg_complaint'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">STATUS LAPORAN</td>
                                    <td class="uppercase">
                                        <?php 
                                            if($dataReport['stts_complaint']=="Pending")
                                            {
                                                echo "<span class='label label-orange'>PENDING</span>";
                                            }
                                            else if ($dataReport['stts_complaint']=="Process") {
                                                echo "<span class='label label-teal'>PROCESS</span>";
                                            }
                                            else if ($dataReport['stts_complaint']=="Clear") {
                                                echo "<span class='label label-green'> SELESAI </span>";
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">TIKOR (LATTITUDE, LONGITUDE)</td>
                                    <td class="uppercase">
                                        <?php echo $dataReport['lat_complaint'].", ".$dataReport['lng_complaint'] ?>
                                        | <a target="_blank" href="https://www.google.co.id/maps/search/<?php echo $dataReport['lat_complaint']?>,<?php echo $dataReport['lng_complaint'] ?>"><span class='clip-map'> Get Location </span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">PIC</td>
                                    <td class="uppercase">
                                        <?php 
                                            $hp_pic = $dataReport['pic_complaint'];

                                            if($dataReport['pic_complaint']=="")
                                            {
                                                echo "-"; 
                                            }
                                            else
                                            {
                                                foreach($data_petugas->result_array() as $d)
                                                {
                                                    if($d['wa']==$hp_pic)
                                                    {
                                                        echo $d['nama']." (".$d['wa'].")";
                                                        break;
                                                    }
                                                }
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right"><a data-toggle="modal" href="#" data-target="#modalAttach<?php echo $dataReport['uuid_complaint'] ?>"><img class="animated heartBeat infinite" src="<?php echo base_url(); ?>upload/complaint/<?php echo $dataReport['attach'] ?>" style=" border-radius: 10px; width:100%; height:250px;"></a></td>
                                    <td class="uppercase">
                                        <textarea disabled style="resize:none;width:100%;height:250px;"><?php echo $dataReport['isi_laporan'] ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">TAKE ACTION</td>
                                    <td align="right">
                                        <?php 
                                            if($dataReport['stts_complaint']=="Pending")
                                            {
                                                echo "
                                                    <a data-toggle='modal' href='#modalPilihPetugas' class='btn btn-md btn-blue tooltips' data-placement='top' data-original-title='PROCESS NOW!''><i class='fa fa-cogs'></i> PROCESS NOW !</a>";
                                            }
                                            else if ($dataReport['stts_complaint']=="Process") {
                                                echo "
                                                    <a href='".base_url()."index.php/panel/closeComplaint/".$dataReport['uuid_complaint']."' class='btn btn-md btn-green tooltips' data-placement='top' data-original-title='CLOSE THIS REPORT!'><i class='clip-checkmark-2'></i> DONE, CLOSE NOW! </a>";
                                            }
                                            else if ($dataReport['stts_complaint']=="Clear") {
                                                echo "<a href='#' disabled class='btn btn-md btn-green tooltips' data-placement='top' data-original-title='GANGGUAN CLEAR'><i class='clip-checkmark-2'></i> CLOSED! </a>";
                                            }
                                        ?>
                                        
                                        
                                    </td>
                                </tr>
                               
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php echo $modalPilihPetugas; ?>
            <?php echo $modalAttach; ?>
			<!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php $this->load->view('versi'); ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    
    
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

     <?php echo $this->session->flashdata('info2'); ?>

    <script>
        jQuery(document).ready(function() {
            Main.init(); 
			UIElements.init();
			UIModals.init();
            UIButtons.init();
        });
    </script>

</body>

</html>