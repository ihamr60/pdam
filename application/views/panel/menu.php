<ul class="main-navigation-menu">
    
    <li>
        <a 
            <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: #346da4; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/panel/bg_home?home=1">
            <i class="clip-home-3" <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>DASHBOARD</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['pengumuman']) && $_GET['pengumuman']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/panel/bg_pengumuman?pengumuman=1">
            <i class="clip-file-2" <?php if (isset($_GET['pengumuman']) && $_GET['pengumuman']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>PENGUMUMAN</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['lapPelanggan']) && $_GET['lapPelanggan']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/panel/bg_lapPelanggan?lapPelanggan=1">
            <i class="clip-data" <?php if (isset($_GET['lapPelanggan']) && $_GET['lapPelanggan']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>LAP PELANGGAN</b></span>
            <b>
                <?php  
                    if($pendingPelanggan['TOTAL']!=0)
                    {
                        echo "<span class='badge badge-orange'>".$pendingPelanggan['TOTAL']."</span>";
                    }
                ?>
            </b>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['lapPublik']) && $_GET['lapPublik']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/panel/bg_lapPublik?lapPublik=1">
            <i class="clip-data" <?php if (isset($_GET['lapPublik']) && $_GET['lapPublik']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>LAP MASYARAKAT</b></span>
                <b>
                    <?php  
                        if($pendingNonPelanggan['TOTAL']!=0)
                        {
                            echo "<span class='badge badge-orange'>".$pendingNonPelanggan['TOTAL']."</span>";
                        }
                    ?>
                </b>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['pasangBaru']) && $_GET['pasangBaru']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/panel/bg_pasangBaru?pasangBaru=1">
            <i class="fa fa-industry" <?php if (isset($_GET['pasangBaru']) && $_GET['pasangBaru']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>PASANG BARU</b></span>
            <b>
                <?php  
                    if($pendingPasangBaru['TOTAL']!=0)
                    {
                        echo "<span class='badge badge-orange'>".$pendingPasangBaru['TOTAL']."</span>";
                    }
                ?>
            </b>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['lokasiLoket']) && $_GET['lokasiLoket']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/panel/bg_lokasiLoket?lokasiLoket=1">
            <i class="clip-map" <?php if (isset($_GET['lokasiLoket']) && $_GET['lokasiLoket']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>DENAH LOKET</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['artikel']) && $_GET['artikel']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/panel/bg_artikel?artikel=1">
            <i class="clip-pencil-2" <?php if (isset($_GET['artikel']) && $_GET['artikel']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>ARTIKEL</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['petugasLap']) && $_GET['petugasLap']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/panel/bg_petugasLap?petugasLap=1">
            <i class="clip-users" <?php if (isset($_GET['petugasLap']) && $_GET['petugasLap']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>DATA PETUGAS</b></span>
        </a>
    </li>
    
   <!-- <li>
        <a href="javascript:void(0)">
            <i class="clip-file"></i>
            <span class="title"><b>Data Pasien</b></span><i class="icon-arrow"></i>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo base_url();?>index.php/panel/bg_dataCovid">
                    <i class="clip-file-2"></i>
                    <span class="title">Data Covid-19 </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url();?>index.php/panel/bg_dataOtgDesa">
                    <i class="clip-file-2"></i>
                    <span class="title">Data OTG Desa</span>
                </a>
            </li>
        </ul>
    </li> -->
    
</ul>