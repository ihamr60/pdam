<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]--><head>
    <title>Panel E-PDAM Langsa</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

   
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
    <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="clip-home-3"></i>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Laporan Pasang Baru
                            </li>
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> REKAP LAPORAN PASANG BARU
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                                    <a data-original-title="RESIZE FULL" data-content="Klik icon tersebut untuk tampilan full table" data-placement="top" data-trigger="hover" id="test" class="btn btn-xs btn-link panel-expand popovers" href="#"> <i class="fa fa-laptop"></i> </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <!--<div class="row">
                                    <div class="col-md-12 space20">
                                        <a data-toggle="modal" data-target="#tambahData" class="btn btn-orange">
                                            Tambahkan Pekerjaan <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div> -->
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="10" class="col-to-export">#</th>
                                                <th width="10" class="col-to-export center">ID REG</th>
                                                <th width="50" class="col-to-export">ID KTP</th>
                                                <th width="140" class="col-to-export">NAMA PEMOHON</th>
                                                <th width="100" class="col-to-export center">NO. HP</th>
                                                <th class="col-to-export">ALAMAT</th>
                                                <th class="col-to-export center">DESA</th>
                                                <th class="col-to-export">KEC.</th>
                                                <th class="col-to-export">STATUS</th>
                                                <th class="col-to-export center">TERBIT</th>
                                                <th class="col-to-export center">PIC</th>
                                                <th class="col-to-export center">SELESAI</th>
                                                <th class="center">#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                function limit_words($string, $word_limit){
                                                    $words = explode(" ",$string);
                                                    return implode(" ",array_splice($words,0,$word_limit));
                                                }
                                                foreach ($data_pasangBaru->result_array() as $d)
                                                {
                                            ?>

                                                <?php 
                                                    if($d['stts_pasang_baru']=="Pending")
                                                    {
                                                ?>
                                                    <tr style="background-color: #f99191;">
                                                        <td align="center"><?php echo $d['id_reg'] ?></td>
                                                        <td align="center">REG-<?php echo $d['id_reg'] ?></td>
                                                        <td align="right"><b><?php echo $d['ktp_pemohon'] ?></b></td>
                                                        <td style="text-transform: uppercase;"><b><?php echo $d['nama_pemohon'] ?></b></td>
                                                        <td class="center"><?php echo $d['hp_pemohon'] ?></td>
                                                        <td class="center"><?php echo $d['alamat_pemohon'] ?></td>
                                                        <td class="center"><?php echo $d['Desa'] ?></td>
                                                        <td class="center"><?php echo $d['Kecamatan'] ?></td>
                                                        <td><?php 
                                                                if($d['stts_pasang_baru']=="Pending")
                                                                {
                                                                    echo "<span class='label label-orange'>PENDING</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Process") {
                                                                    echo "<span class='label label-teal'>PROCESS</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Clear") {
                                                                    echo "<span class='label label-green'> SELESAI </span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Instalasi") {
                                                                    echo "<span class='label label-info'> INSTALASI </span>";
                                                                }
                                                            ?></td>
                                                        
                                                        <td class="center"><b><?php echo date('d/m/Y', strtotime($d['created_at_pasang_baru'])); ?></b></td>
                                                        <td class="center" style="text-transform: uppercase;"><b><?php 
                                                            if($d['nm_pic_pasang_baru']!="")
                                                            {
                                                                echo $d['nm_pic_pasang_baru'];
                                                            }
                                                            else
                                                            {
                                                                echo "-";
                                                            }
                                                         ?></b></td>
                                                         <td class="center"><b>
                                                             <?php 
                                                            if($d['finished_at_pasang_baru']!="")
                                                            {
                                                                echo date('d/m/Y', strtotime($d['finished_at_pasang_baru']));
                                                            }
                                                            else
                                                            {
                                                                echo "<span class='label label-danger'> Belum Selesai </span>";
                                                            }
                                                         ?></b>
                                                         </td>
                                                        <td align="center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                </a>
                                                                <ul role="menu" class="dropdown-menu pull-right">
                                                                    <li role="presentation">
                                                                        <a data-toggle='modal' href='#modalPilihPetugas<?php echo $d['uuid_pasang_baru'] ?>' role="menuitem" tabindex="-1" href="#">
                                                                            <i class="fa fa-share"></i> Process Survey
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url() ?>upload/pasang_baru/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="clip-file-2"></i> Foto KTP
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="https://www.google.co.id/maps/search/<?php echo $d['lat_pemohon'] ?>,<?php echo $d['lng_pemohon'] ?>/">
                                                                            <i class="fa fa-map"></i> Location
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" onclick="return confirm('Anda yakin akan menghapus data REG-<?php echo $d['id_reg'] ?>..???')" href="<?php echo base_url() ?>index.php/panel/deletePasangBaru/<?php echo $d['uuid_pasang_baru'] ?>/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="fa fa-times"></i> Hapus
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>    
                                                        </td>
                                                        
                                                    </tr>
                                            <?php   } ?>
                                                <?php 
                                                    if($d['stts_pasang_baru']=="Clear")
                                                    {
                                                ?>
                                                    <tr style="background-color: #65d462;">
                                                        <td align="center"><?php echo $d['id_reg'] ?></td>
                                                        <td align="center">REG-<?php echo $d['id_reg'] ?></td>
                                                        <td align="right"><b><?php echo $d['ktp_pemohon'] ?></b></td>
                                                        <td style="text-transform: uppercase;"><b><?php echo $d['nama_pemohon'] ?></b></td>
                                                        <td class="center"><?php echo $d['hp_pemohon'] ?></td>
                                                        <td class="center"><?php echo $d['alamat_pemohon'] ?></td>
                                                        <td class="center"><?php echo $d['Desa'] ?></td>
                                                        <td class="center"><?php echo $d['Kecamatan'] ?></td>
                                                        <td><?php 
                                                                if($d['stts_pasang_baru']=="Pending")
                                                                {
                                                                    echo "<span class='label label-orange'>PENDING</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Process") {
                                                                    echo "<span class='label label-teal'>PROCESS</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Clear") {
                                                                    echo "<span class='label label-green'> SELESAI </span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Instalasi") {
                                                                    echo "<span class='label label-info'> INSTALASI </span>";
                                                                }
                                                            ?></td>
                                                        
                                                        <td class="center"><b><?php echo date('d/m/Y', strtotime($d['created_at_pasang_baru'])); ?></b></td>
                                                        <td class="center" style="text-transform: uppercase;"><b><?php 
                                                            if($d['nm_pic_pasang_baru']!="")
                                                            {
                                                                echo $d['nm_pic_pasang_baru'];
                                                            }
                                                            else
                                                            {
                                                                echo "-";
                                                            }
                                                         ?></b></td>
                                                         <td class="center"><b>
                                                             <?php 
                                                            if($d['finished_at_pasang_baru']!="")
                                                            {
                                                                echo "<span class='label label-green'>".date('d / m / Y', strtotime($d['finished_at_pasang_baru']))."</span>";
                                                            }
                                                            else
                                                            {
                                                                echo "<span class='label label-danger'> Belum Selesai </span>";
                                                            }
                                                         ?></b>
                                                         </td>
                                                        <td align="center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                </a>
                                                                <ul role="menu" class="dropdown-menu pull-right">
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url() ?>upload/pasang_baru/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="clip-file-2"></i> Foto KTP
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="https://www.google.co.id/maps/search/<?php echo $d['lat_pemohon'] ?>,<?php echo $d['lng_pemohon'] ?>/">
                                                                            <i class="fa fa-map"></i> Location
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" onclick="return confirm('Anda yakin akan menghapus data REG-<?php echo $d['id_reg'] ?>..???')" href="<?php echo base_url() ?>index.php/panel/deletePasangBaru/<?php echo $d['uuid_pasang_baru'] ?>/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="fa fa-times"></i> Hapus
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>    
                                                        </td>
                                                        
                                                    </tr>
                                            <?php   } ?>
                                            <?php 
                                                    if($d['stts_pasang_baru']=="Process")
                                                    {
                                                ?>
                                                    <tr style="background-color: #93e1f7;">
                                                        <td align="center"><?php echo $d['id_reg'] ?></td>
                                                        <td align="center">REG-<?php echo $d['id_reg'] ?></td>
                                                        <td align="right"><b><?php echo $d['ktp_pemohon'] ?></b></td>
                                                        <td style="text-transform: uppercase;"><b><?php echo $d['nama_pemohon'] ?></b></td>
                                                        <td class="center"><?php echo $d['hp_pemohon'] ?></td>
                                                        <td class="center"><?php echo $d['alamat_pemohon'] ?></td>
                                                        <td class="center"><?php echo $d['Desa'] ?></td>
                                                        <td class="center"><?php echo $d['Kecamatan'] ?></td>
                                                        <td><?php 
                                                                if($d['stts_pasang_baru']=="Pending")
                                                                {
                                                                    echo "<span class='label label-orange'>PENDING</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Process") {
                                                                    echo "<span class='label label-teal'>SURVEY</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Clear") {
                                                                    echo "<span class='label label-green'> SELESAI </span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Instalasi") {
                                                                    echo "<span class='label label-info'> INSTALASI </span>";
                                                                }
                                                            ?></td>
                                                        
                                                        <td class="center"><b><?php echo date('d/m/Y', strtotime($d['created_at_pasang_baru'])); ?></b></td>
                                                        <td class="center" style="text-transform: uppercase;"><b><?php 
                                                            if($d['nm_pic_pasang_baru']!="")
                                                            {
                                                                echo $d['nm_pic_pasang_baru'];
                                                            }
                                                            else
                                                            {
                                                                echo "-";
                                                            }
                                                         ?></b></td>
                                                         <td class="center"><b>
                                                             <?php 
                                                            if($d['finished_at_pasang_baru']!="")
                                                            {
                                                                echo date('d/m/Y', strtotime($d['finished_at_pasang_baru']));
                                                            }
                                                            else
                                                            {
                                                                echo "<span class='label label-danger'> Belum Selesai </span>";
                                                            }
                                                         ?></b>
                                                         </td>
                                                        <td align="center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                </a>
                                                                <ul role="menu" class="dropdown-menu pull-right">
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>index.php/panel/nextInstalasi/<?php echo $d['uuid_pasang_baru'] ?>">
                                                                            <i class="clip-file-2"></i> Lanjut Instalasi
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>index.php/panel/tolakPasangBaru/<?php echo $d['uuid_pasang_baru'] ?>">
                                                                            <i class="clip-file-2"></i> Tolak Permohonan
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url() ?>upload/pasang_baru/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="clip-file-2"></i> Foto KTP
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="https://www.google.co.id/maps/search/<?php echo $d['lat_pemohon'] ?>,<?php echo $d['lng_pemohon'] ?>/">
                                                                            <i class="fa fa-map"></i> Location
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" onclick="return confirm('Anda yakin akan menghapus data REG-<?php echo $d['id_reg'] ?>..???')" href="<?php echo base_url() ?>index.php/panel/deletePasangBaru/<?php echo $d['uuid_pasang_baru'] ?>/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="fa fa-times"></i> Hapus
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>    
                                                        </td>
                                                        
                                                    </tr>
                                            <?php   } ?>
                                            <?php 
                                                    if($d['stts_pasang_baru']=="Instalasi")
                                                    {
                                                ?>
                                                    <tr style="background-color: #15999b;">
                                                        <td align="center"><?php echo $d['id_reg'] ?></td>
                                                        <td align="center">REG-<?php echo $d['id_reg'] ?></td>
                                                        <td align="right"><b><?php echo $d['ktp_pemohon'] ?></b></td>
                                                        <td style="text-transform: uppercase;"><b><?php echo $d['nama_pemohon'] ?></b></td>
                                                        <td class="center"><?php echo $d['hp_pemohon'] ?></td>
                                                        <td class="center"><?php echo $d['alamat_pemohon'] ?></td>
                                                        <td class="center"><?php echo $d['Desa'] ?></td>
                                                        <td class="center"><?php echo $d['Kecamatan'] ?></td>
                                                        <td><?php 
                                                                if($d['stts_pasang_baru']=="Pending")
                                                                {
                                                                    echo "<span class='label label-orange'>PENDING</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Process") {
                                                                    echo "<span class='label label-teal'>PROCESS</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Clear") {
                                                                    echo "<span class='label label-green'> SELESAI </span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Instalasi") {
                                                                    echo "<span class='label label-info'> INSTALASI </span>";
                                                                }
                                                            ?></td>
                                                        
                                                        <td class="center"><b><?php echo date('d/m/Y', strtotime($d['created_at_pasang_baru'])); ?></b></td>
                                                        <td class="center" style="text-transform: uppercase;"><b><?php 
                                                            if($d['nm_pic_pasang_baru']!="")
                                                            {
                                                                echo $d['nm_pic_pasang_baru'];
                                                            }
                                                            else
                                                            {
                                                                echo "-";
                                                            }
                                                         ?></b></td>
                                                         <td class="center"><b>
                                                             <?php 
                                                            if($d['finished_at_pasang_baru']!="")
                                                            {
                                                                echo date('d/m/Y', strtotime($d['finished_at_pasang_baru']));
                                                            }
                                                            else
                                                            {
                                                                echo "<span class='label label-danger'> Belum Selesai </span>";
                                                            }
                                                         ?></b>
                                                         </td>
                                                        <td align="center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                </a>
                                                                <ul role="menu" class="dropdown-menu pull-right">
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>index.php/panel/selesaiPasangBaru/<?php echo $d['uuid_pasang_baru'] ?>">
                                                                            <i class="clip-file-2"></i> Selesai Instalasi
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url() ?>upload/pasang_baru/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="clip-file-2"></i> Foto KTP
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="https://www.google.co.id/maps/search/<?php echo $d['lat_pemohon'] ?>,<?php echo $d['lng_pemohon'] ?>/">
                                                                            <i class="fa fa-map"></i> Location
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" onclick="return confirm('Anda yakin akan menghapus data REG-<?php echo $d['id_reg'] ?>..???')" href="<?php echo base_url() ?>index.php/panel/deletePasangBaru/<?php echo $d['uuid_pasang_baru'] ?>/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="fa fa-times"></i> Hapus
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>  
                                                        </td>
                                                        
                                                    </tr>
                                            <?php   } ?>
                                            <?php 
                                                    if($d['stts_pasang_baru']=="Reject")
                                                    {
                                                ?>
                                                    <tr style="background-color: #000000;">
                                                        <td align="center"><font color="white"><?php echo $d['id_reg'] ?></font></td>
                                                        <td align="center"><font color="white">REG-<?php echo $d['id_reg'] ?></font></td>
                                                        <td align="right"><b><font color="white"><?php echo $d['ktp_pemohon'] ?></font></b></td>
                                                        <td style="text-transform: uppercase;"><b><font color="white"><?php echo $d['nama_pemohon'] ?></font></b></td>
                                                        <td class="center"><font color="white"><?php echo $d['hp_pemohon'] ?></font></td>
                                                        <td class="center"><font color="white"><?php echo $d['alamat_pemohon'] ?></font></td>
                                                        <td class="center"><font color="white"><?php echo $d['Desa'] ?></font></td>
                                                        <td class="center"><font color="white"><?php echo $d['Kecamatan'] ?></font></td>
                                                        <td><font color="white"><?php 
                                                                if($d['stts_pasang_baru']=="Pending")
                                                                {
                                                                    echo "<span class='label label-orange'>PENDING</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Process") {
                                                                    echo "<span class='label label-teal'>PROCESS</span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Clear") {
                                                                    echo "<span class='label label-green'> SELESAI </span>";
                                                                }
                                                                else if ($d['stts_pasang_baru']=="Reject") {
                                                                    echo "<span class='label label-purple'> DITOLAK </span>";
                                                                }
                                                            ?></font></td>
                                                        
                                                        <td class="center"><b><font color="white"><?php echo date('d/m/Y', strtotime($d['created_at_pasang_baru'])); ?></font></b></td>
                                                        <td class="center" style="text-transform: uppercase;"><b><font color="white"><span class='label label-purple'> DITOLAK </span></font></b></td>
                                                         <td class="center"><b><font color="white">
                                                             <?php 
                                                            if($d['finished_at_pasang_baru']!="")
                                                            {
                                                                echo date('d/m/Y', strtotime($d['finished_at_pasang_baru']));
                                                            }
                                                            else
                                                            {
                                                                echo "<span class='label label-purple'> DITOLAK </span>";
                                                            }
                                                         ?></font></b>
                                                         </td>
                                                        <td align="center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                </a>
                                                                <ul role="menu" class="dropdown-menu pull-right">
                                                                    <li role="presentation">
                                                                        <a data-toggle='modal' href='#modalPilihPetugas<?php echo $d['uuid_pasang_baru'] ?>' role="menuitem" tabindex="-1">
                                                                            <i class="clip-file-2"></i> Prosess Survey Ulang
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url() ?>upload/pasang_baru/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="clip-file-2"></i> Foto KTP
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" target="_blank" href="https://www.google.co.id/maps/search/<?php echo $d['lat_pemohon'] ?>,<?php echo $d['lng_pemohon'] ?>/">
                                                                            <i class="fa fa-map"></i> Location
                                                                        </a>
                                                                    </li>
                                                                    <li role="presentation">
                                                                        <a role="menuitem" tabindex="-1" onclick="return confirm('Anda yakin akan menghapus data REG-<?php echo $d['id_reg'] ?>..???')" href="<?php echo base_url() ?>index.php/panel/deletePasangBaru/<?php echo $d['uuid_pasang_baru'] ?>/<?php echo $d['img_ktp'] ?>">
                                                                            <i class="fa fa-times"></i> Hapus
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div> 
                                                        </td>
                                                        
                                                    </tr>
                                            <?php   } ?>

                                        <?php   } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php echo $modalPetugasPasangBaru; ?>
			<!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php $this->load->view('versi'); ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    
    
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

     <?php echo $this->session->flashdata('info2'); ?>

    <script>
        jQuery(document).ready(function() {
            Main.init(); 
			UIElements.init();
            TableExport.init();
			UIModals.init();
            UIButtons.init();
        });
    </script>

</body>

</html>