<script>
    var camera2 = document.getElementById('camera2');
    var frame2 = document.getElementById('frame2');

    camera2.addEventListener('change', function(e) {
      var file2 = e.target.files[0]; 
      // Do something with the image file.
      frame2.src = URL.createObjectURL(file2);
    });
  </script>