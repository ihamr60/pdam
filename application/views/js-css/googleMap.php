<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyAxS2cHzZCAgptfXTezX7bGYChIHvY6xuk"></script> 

<style type="text/css">
	 #googleMap_pelanggan{
        display: block;
        width: 95%;
        height: 350px;
        margin: 0 auto;
        -moz-box-shadow: 0px 5px 20px #ccc;
        -webkit-box-shadow: 0px 5px 20px #ccc;
        box-shadow: 0px 5px 20px #ccc;}

        #googleMap_nonPelanggan{
        display: block;
        width: 95%;
        height: 350px;
        margin: 0 auto;
        -moz-box-shadow: 0px 5px 20px #ccc;
        -webkit-box-shadow: 0px 5px 20px #ccc;
        box-shadow: 0px 5px 20px #ccc;}

        #googleMap_pemohon{
        display: block;
        width: 95%;
        height: 350px;
        margin: 0 auto;
        -moz-box-shadow: 0px 5px 20px #ccc;
        -webkit-box-shadow: 0px 5px 20px #ccc;
        box-shadow: 0px 5px 20px #ccc;}
</style>

<script> 
    // variabel global marker
    var marker;

      
    function taruhMarker(peta, posisiTitik){
        
        if( marker ){
          // pindahkan marker
          marker.setPosition(posisiTitik);
        } else {
          // buat marker baru
          marker = new google.maps.Marker({
            position: posisiTitik,
            map: peta
          });
        }
      
         // isi nilai koordinat ke form
        document.getElementById("lat").value = posisiTitik.lat();
        document.getElementById("lng").value = posisiTitik.lng();
        
      
    }

    function initialize() {
       var propertiPeta = {
          center:new google.maps.LatLng(4.485740, 97.963802),
          zoom:13,
          mapTypeId:google.maps.MapTypeId.ROADMAP
         };
      
        var peta = new google.maps.Map(document.getElementById("googleMap_pelanggan"), propertiPeta);
      
        // even listner ketika peta diklik
        google.maps.event.addListener(peta, 'click', function(event) {
          taruhMarker(this, event.latLng);
      
        });

    }

    
      // event jendela di-load  
    google.maps.event.addDomListener(window, 'load', initialize);

    </script>

    <script> 
    // variabel global marker
    var marker2;

      
    function taruhMarker_nonPelanggan(peta, posisiTitik){
        
        if( marker2 ){
          // pindahkan marker
          marker2.setPosition(posisiTitik);
        } else {
          // buat marker baru
          marker2 = new google.maps.Marker({
            position: posisiTitik,
            map: peta
          });
        }
      
         // isi nilai koordinat ke form
        document.getElementById("lat2").value = posisiTitik.lat();
        document.getElementById("lng2").value = posisiTitik.lng();
        
      
    }

    function initialize_nonPelanggan() {
       var propertiPeta = {
          center:new google.maps.LatLng(4.485740, 97.963802),
          zoom:13,
          mapTypeId:google.maps.MapTypeId.ROADMAP
         };
      
        var peta = new google.maps.Map(document.getElementById("googleMap_nonPelanggan"), propertiPeta);
      
        // even listner ketika peta diklik
        google.maps.event.addListener(peta, 'click', function(event) {
          taruhMarker_nonPelanggan(this, event.latLng);
      
        });

    }

    
      // event jendela di-load  
    google.maps.event.addDomListener(window, 'load', initialize_nonPelanggan);

    </script>

    <script> 
    // variabel global marker PEmohon pasang baru
    var marker3;

      
    function taruhMarker_pemohon(peta, posisiTitik){
        
        if( marker3 ){
          // pindahkan marker
          marker3.setPosition(posisiTitik);
        } else {
          // buat marker baru
          marker3 = new google.maps.Marker({
            position: posisiTitik,
            map: peta
          });
        }
      
         // isi nilai koordinat ke form
        document.getElementById("lat3").value = posisiTitik.lat();
        document.getElementById("lng3").value = posisiTitik.lng();
        
      
    }

    function initialize_pemohon() {
       var propertiPeta = {
          center:new google.maps.LatLng(4.485740, 97.963802),
          zoom:13,
          mapTypeId:google.maps.MapTypeId.ROADMAP
         };
      
        var peta = new google.maps.Map(document.getElementById("googleMap_pemohon"), propertiPeta);
      
        // even listner ketika peta diklik
        google.maps.event.addListener(peta, 'click', function(event) {
          taruhMarker_pemohon(this, event.latLng);
      
        });

    }

    
      // event jendela di-load  
    google.maps.event.addDomListener(window, 'load', initialize_pemohon);

    </script>