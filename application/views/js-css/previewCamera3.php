<script>
    var camera3 = document.getElementById('camera3');
    var frame3 = document.getElementById('frame3');

    camera3.addEventListener('change', function(e) {
      var file3 = e.target.files[0]; 
      // Do something with the image file.
      frame3.src = URL.createObjectURL(file3);
    });
  </script>