<script>
    var camera4 = document.getElementById('camera4');
    var frame4 = document.getElementById('frame4');

    camera4.addEventListener('change', function(e) {
      var file4 = e.target.files[0]; 
      // Do something with the image file.
      frame4.src = URL.createObjectURL(file4);
    });
  </script>