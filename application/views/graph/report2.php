<!--<script type="text/javascript">
    // Build the chart
Highcharts.chart('report2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'PERSENTASE JENIS KELUHAN'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Persentase',
        colorByPoint: true,
        data: [{
            name: 'Meter Macet',
            y: <?php echo $meterMacet['TOTAL'] ?>,
            sliced: true,
            selected: true
        }, {
            name: 'Kebocoran Pipa',
            y: <?php echo $kebocoranPipa['TOTAL'] ?>
        }, {
            name: 'Lainnya',
            y: <?php echo $keluhanLainnya['TOTAL'] ?>
        }, {
            name: 'Check Meteran',
            y: <?php echo $checkMeteran['TOTAL'] ?>
        }, {
            name: 'Bocor Dekat Meter',
            y: <?php echo $bocorDekatMeter['TOTAL'] ?>
        }, {
            name: 'Air Macet',
            y: <?php echo $airMacet['TOTAL'] ?>
        }]
    }]
});
</script> -->


<script type="text/javascript">
  // Make monochrome colors
var pieColors = (function () {
  var colors = [],
    base = Highcharts.getOptions().colors[0],
    i;

  for (i = 0; i < 10; i += 1) {
    // Start out with a darkened base color (negative brighten), and end
    // up with a much brighter color
    colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
  }
  return colors;
}());

// Build the chart
Highcharts.chart('report2', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'PERSENTASE PER JENIS KELUHAN / LAPORAN BULAN BERJALAN'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      colors: pieColors,
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
        distance: -50,
        filter: {
          property: 'percentage',
          operator: '>',
          value: 4
        }
      }
    }
  },
  series: [{
    name: 'Total',
    data: [
      { name: 'Bocor Pipa', y: <?php echo $kebocoranPipa['TOTAL'] ?> },
      { name: 'Meter Macet', y: <?php echo $meterMacet['TOTAL'] ?> },
      { name: 'Lainnya', y: <?php echo $keluhanLainnya['TOTAL'] ?> },
      { name: 'Check Meter', y: <?php echo $checkMeteran['TOTAL'] ?> },
      { name: 'Bocor Dekat Meter', y: <?php echo $bocorDekatMeter['TOTAL'] ?> },
      { name: 'Air Macet', y: <?php echo $airMacet['TOTAL'] ?> }
    ]
  }]
});
</script>