<script type="text/javascript">

    Highcharts.chart('report1', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'GRAFIK LAPORAN PER BULAN TAHUN <?php echo date('Y') ?>'
        },
        subtitle: {
            text: 'Grafik rekapan per bulan tahun <?php echo date('Y') ?>'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            //color: '#52b251',
            name: 'Keluhan Pelanggan',
            data: <?php echo json_encode($totReport2,TRUE); ?>
        }, {
            name: 'Keluhan Masyarakat',
            data: <?php echo json_encode($totReport1,TRUE); ?>
        }, {
            color: '#52b251',
            name: 'Pemasangan Baru',
            data: <?php echo json_encode($totReport3,TRUE); ?>
        }]
    });
</script>