<?php 
    date_default_timezone_set('Asia/Jakarta');
    $month  = date('m');
?>
<script type="text/javascript">
  // Make monochrome colors
var pieColors = (function () {
  var colors = [],
    base = Highcharts.getOptions().colors[0],
    i;

  for (i = 0; i < 10; i += 1) {
    // Start out with a darkened base color (negative brighten), and end
    // up with a much brighter color
    colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
  }
  return colors;
}());

// Build the chart
Highcharts.chart('report3', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'PERSENTASE PROGRESS LAPORAN BULAN BERJALAN'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      colors: pieColors,
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
        distance: -50,
        filter: {
          property: 'percentage',
          operator: '>',
          value: 4
        }
      }
    }
  },
  series: [{
    name: 'Total',
    data: [
      { name: 'Pending', y: <?php echo $totalPending['TOTAL'] ?> },
      { name: 'On Process', y: <?php echo $totalProcess['TOTAL'] ?> },
      { name: 'Selesai', y: <?php echo $totalClear['TOTAL'] ?> }
    ]
  }]
});
</script>