<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>E-PDAM Langsa</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>vendor/css/framework7.ios.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>vendor/css/ionicons.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>vendor/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>vendor/css/custom.css">

    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />

  <?php //echo $tableStyle ?>
  <?php echo $googleMap ?>
  <?php echo $preLoader ?>

  </head>
  <body class="color-theme-blue">
    <!-- App root element -->
    <div id="app">
      <div class="views tabs">
        <div id='loading'><div id='progress-bar'></div><div id='loader'></div></div>
        <!-- bg_cekTagihan -->
        <?php echo $bg_cekTagihan ?>
        
        <!-- bg_pasangBaru -->
        <?php echo $bg_pasangBaru ?>
       
        <!-- bg_home -->
        <?php echo $bg_home ?>
        <?php echo $bg_post ?>
        
        <!-- bg_petaLoket -->
        <?php echo $bg_petaLoket ?>

        <!-- bg_pengaduan -->
        <?php echo $bg_pengaduan ?>
        <?php echo $bg_pengaduanPelanggan ?>

        <!-- bg_menuComplaint -->
        <?php echo $bg_menuComplaint ?>

       

        <!-- bg_status -->
        <?php echo $bg_status ?>

        <!-- menuBawah -->
        <?php echo $menuBawah ?>
        
      </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>



    <script src="<?php echo base_url() ?>vendor/js/framework7.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/js/app.js"></script>

    <?php echo $this->session->flashdata('infoLunas'); ?>
    <?php echo $this->session->flashdata('info2'); ?>

    <script type="text/javascript">
        // Notification feature / Create the notification
        var myNotification = app.notification.create({
          icon: '<i class="icon ion-ios-notifications"></i>',
          title: 'Pemberitahuan info terkini',
          subtitle: 'INFO: <?php echo $info_gangguan['judul_gangguan'] ?>',
          text: 'Updated at <?php echo date('d/m/Y H:i', strtotime($info_gangguan['gangguan_updated_at'])) ?><br<br><font size="0"><?php echo $info_gangguan['detail_gangguan'] ?></font>',
          closeButton: true,
        });

        // Attach the notification event to all elements that have the open-notification class
        $$('.open-notification').on('click', function () {
          myNotification.open();
        });
    </script>
  </body>
</html>
