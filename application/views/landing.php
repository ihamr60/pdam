<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>E-PDAM Langsa</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600&amp;subset=latin-ext" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url();?>vendor/css/framework7.ios.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>vendor/css/ionicons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>vendor/css/style.css">
    
    <!-- CSS KHUSUS HALAMAN INI -->
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css'>
    <link href="<?php echo base_url();?>vendor/css/main.css" rel="stylesheet">   
  </head>
  <body>
    <!-- App root element -->
   
      <!-- Options headline effects: .rotate | .slide | .zoom | .push | .clip -->
            <section class="hero-section hero-section--image clearfix clip">
                <div class="hero-section__wrap">
                    <div class="hero-section__option">
                        <img src="<?php echo base_url();?>vendor/img/pdam/air.jpg" width="100%" height="100%">
                    </div>
                    <!-- .hero-section__option -->

                    <div class="container">
                        <div class="row">
                            <div class="offset-lg-0 col-lg-8">
                                <div class="title-01 title-01--11 text-center">
                                <div align="center"><img src="<?php echo base_url();?>vendor/img/pdam/logo.png" width="120px" height="120px"></div>
                                    <h2 class="title__heading">
                                        <span>PDAM<br></span>
                                        <strong class="hero-section__words">
                                            <span class="title__effect is-visible">Langsa</span>
                                            <span class="title__effect">Melayani</span>
                                            <span class="title__effect">Mengaliri</span>
                                        </strong>
                                    </h2>
                                    <div class="title__description">Media ini berperan sebagai keterbukaan PDAM kepada pelanggan & masyarakat umum terkait pelayanan dan informasi penting lainnya</div>
                                   <form method="POST" action="<?php echo base_url();?>index.php/welcome/bg_utama?bg_home=1">
        <br/>
 <!-- Options btn color: .btn-success | .btn-info | .btn-warning | .btn-danger | .btn-primary -->
        <button class="btn" style="background-color: #0481c9; border-color: #0481c9;" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing ">WELCOME</button>
       </form>
       <br>
       <font size="0">Copyright 2020. PDAM Tirta Keumuning Langsa.<br>Supported by Diskominfo Langsa</font><br>
       <!--<div style="margin:3em;">
<button type="button" class="btn btn-primary btn-lg " id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing Order">Submit Order with Circle</button>
<br>
  <br>
<button type="button" class="btn btn-primary btn-lg" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order">Submit Order with Spinner</button>
<br>
  <br>
<button type="button" class="btn btn-primary btn-lg" id="load" data-loading-text="<i class='fa fa-refresh fa-spin '></i> Processing Order">Submit Order with Refresh</button>
</div> -->
       
                                   
                                </div>
                                <!-- .title-01 -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- .hero-section -->

    <script src="<?php echo base_url();?>vendor/js/framework7.min.js"></script>
    <script src="<?php echo base_url();?>vendor/js/app.js"></script>
    
    <!-- JS KHUSUS HALAMAN INI -->
     <script src="<?php echo base_url();?>vendor/js/vendor/modernizr-2.8.3.min.js"></script>
     <script src="<?php echo base_url();?>vendor/js/vendor/jquery-1.12.0.min.js"></script>
     <script src="<?php echo base_url();?>vendor/js/plugins/animate-headline.js"></script>
     <script src="<?php echo base_url();?>vendor/js/main.js"></script>

     <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/bootstrap.min.js'></script>
<script>
$('.btn').on('click', function() {
    var $this = $(this);
  $this.button('loading');
    setTimeout(function() {
       $this.button('reset');
   }, 9000);
});
</script>
        
  </body>
</html>
