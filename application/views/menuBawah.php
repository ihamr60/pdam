<div class="toolbar tabbar tabbar-labels">
  <div class="toolbar-inner">

  
    <a href="#view-today" class="tab-link <?php echo $this->session->flashdata('bg-cekTagihan-link-active'); ?>">
      <img class="" src="<?php echo base_url(); ?>vendor/img/pdam/open.png" style=" width:25px; height:25px;">
      <span class="tabbar-label">Check Tagihan</span>
    </a>
    <a href="#view-categories" class="tab-link <?php echo $this->session->flashdata('bg-pasangBaru-link-active'); ?>">
      <img class="" src="<?php echo base_url(); ?>vendor/img/pdam/security.png" style=" width:25px; height:25px;">
      <span class="tabbar-label">Pasang Baru</span>
    </a>
    <a href="#view-discover" class="tab-link <?php echo $this->session->flashdata('bg-home-link-active'); ?>">
      <img class="animated heartBeat infinite" src="<?php echo base_url(); ?>vendor/img/pdam/home.png" style=" width:25px; height:25px;">
      <span class="tabbar-label">Home</span>
    </a>
    <a href="#view-search" class="tab-link">
      <img class="" src="<?php echo base_url(); ?>vendor/img/pdam/pin.png" style=" width:25px; height:25px;">
      <span class="tabbar-label">Lokasi Loket</span>
    </a>
    <a href="#view-pages" class="tab-link 
      <?php echo $this->session->flashdata('bg-complaintPelanggan-link-active'); ?>
      <?php echo $this->session->flashdata('bg-status-link-active'); ?>">
      <img class="" src="<?php echo base_url(); ?>vendor/img/pdam/chat.png" style=" width:25px; height:25px;">
      <span class="tabbar-label">Pengaduan</span>
    </a>
  </div>
</div>