<!-- Search View -->
        <div id="view-pages" class="view tab animated fadeIn">
          <div  data-name="author" class="page author no-navbar">
            <div class="page-content">
              <div class="discover-gradient">
                <svg viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="white" points="0,100 100,0 0,100"></svg>
                <img src="<?php echo base_url(); ?>vendor/img/pdam/call_center.jpg" height="100%" width="100%;">
              </div>
              <div class="block">
                <div class="categories-container"  style="margin-top:250px">
                  <a href="#view-formComplaint" class="tab-link">
                    <div class="category card" style="background-image: url('<?php echo base_url(); ?>vendor/img/pdam/complaint.jpg'); box-shadow: 0px 35px 20px -10px rgba(252, 186, 47, 0.5);">
                      <h2>Complaint</h2>
                    </div>
                  </a>
                  <a href="#view-status" class="tab-link">
                    <div class="category card" style="background-image: url('<?php echo base_url(); ?>vendor/img/pdam/status.jpg'); box-shadow: 0px 35px 20px -10px rgba(128, 213, 250, 0.5);">
                      <h2>Status</h2>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>