<div id="view-formComplaintPelanggan" class="view tab <?php echo $this->session->flashdata('bg-complaintPelanggan-active') ?>">
  <div data-name="pages" class="page">
  
    <!-- Scrollable page content -->
    <div class="page-content">
      <a href="#view-pages" class="link back close-button tab-link animated infinite pulse">
        <img src="<?php echo base_url() ?>vendor/img/close.svg" alt="Close">
      </a>
     <!-- <div class="oval"></div> -->
     <img class="" src="<?php echo base_url() ?>vendor/img/pdam/air2.jpg" height="250px" width="100%">
      <div class="block">
        <form method="post" action="<?php echo base_url();?>index.php/welcome/addComplaint" enctype="multipart/form-data">
        <div class="contact-container">
          <div class="contact-icon"><img class="animated bounceIn" src="<?php echo base_url() ?>vendor/img/pdam/interface.png" height="50px" width="50px"></div>
          <h3 class="animated pulse">PELANGGAN</h3>
          <div class="list no-hairlines custom-form contact-form">

          <p align="center"><font size="2"><b>Note:</b> Kami menjamin data pelapor tidak akan dipublikasikan. Pilih jenis pengaduan dibawah:</font></p>
            <ul>
              <?php echo $menuAtas_pengaduan ?>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input 
                      type="number" 
                      name="id_pelapor" 
                      placeholder="*ID Pelanggan (Ex:00239)"
                      required>
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input 
                    required 
                    type="number" 
                    name="hp_pelapor"
                    placeholder="*No. Handphone / Telephone">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input 
                     
                    type="text"
                    name="email_pelapor" 
                    placeholder="Email (Optional)">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <select 
                      required
                      name="kateg_complaint">
                      <option value="">*Pilih Kategori</option>
                      <option value="Kebocoran Pipa">Kebocoran Pipa</option>
                      <option value="Bocor Dekat Meter">Bocor Dekat Meter</option>
                      <option value="Air Macet">Air Macet</option>
                      <option value="Meter Macet">Meter Macet</option>
                      <option value="Check Meteran">Check Meteran</option>
                      <option value="Keluhan Lainnya">Lainnya</option>
                    </select>
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <textarea  
                    required 
                    class="resizable" 
                    name="isi_laporan"
                    placeholder="*Tuliskan detail keluhan / laporan Anda disini.."></textarea>
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              
               Tandai Lokasi Gangguan:
               <p></p>
               <div id="googleMap_pelanggan" 
                  style="
                    width:100%; 
                    height:350px; 
                    border-radius:10px; 
                    border: 0px solid black;">      
                </div>
                <input type="hidden" id="lat" name="lat_complaint" value="" required> 
                <input type="hidden" id="lng" name="lng_complaint" value="" required>

                     <br>
              Lampiran: <br>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                 
                    <input 
                      type="file" 
                      accept="image/*" 
                      name="attach" 
                      capture="camera2" 
                      id="camera2">
                    <span class="input-clear-button"></span>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">           
                  <img id="frame2" width="100%" height="100%" src="<?php echo base_url() ?>vendor/img/pdam/preview.png" alt="Preview" style="border-radius: 40px">
                  <?php echo $previewCamera2 ?> 
                </div>
              </li>
            </ul>
          </div>
          <button id="myButton" data-loading-text="LOADING..." class="big-button button button-fill link"><i class="icon ion-ios-send"></i>SUBMIT</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <?php echo $preLoader ?>
</div>

<script type="text/javascript">
  $("textarea").keydown(function(e){
    // Enter was pressed without shift key
    if (e.keyCode == 13 && !e.shiftKey)
    {
        // prevent default behavior
        e.preventDefault();
    }
    });
</script>
