<?php 
  foreach($data_allArtikel->result_array() as $d){
?>

<div id="view-post<?php echo $d['uuid'] ?>" class="page single single-1 no-navbar view tab" data-name="single">
  <div class="page-content">
    <a href="#view-discover" class="tab-link link back close-button  animated infinite pulse" data-dismiss="modal"> <!-- Hide pake modal -->
      <img src="<?php echo base_url() ?>vendor/img/close.svg" alt="Close">
    </a>
    <img class="cover-image" src="<?php echo base_url() ?>upload/artikel/<?php echo $d['img'] ?>" alt="">
    <div class="block article">
      <div class="post-infos">
        <div class="post-category"><?php echo $d['kategori_artikel'] ?></div>
        <div class="post-date">Posted <?php echo date('d-F-Y', strtotime($d['artikel_updated_at'])) ?></div>
      </div>
      <h1><?php echo $d['judul'] ?></h1>
      <?php echo $d['konten'] ?>
    </div>
    <div class="block">
      <div class="author-block">
        <a href="/author/" class="link">
          <img src="<?php echo base_url() ?>vendor/img/pdam/avatar.png" alt="">
          <div class="author-infos">
            <div class="author-name"><?php echo $d['author'] ?></div>
            <div class="author-description">Administrator</div>
          </div>
        </a>
      </div>
      <!--
      <div class="title-medium-container">
        <h2>Share This Post</h2>
      </div>
      <div class="social-buttons">
        <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themeforest.net" target="_blank" class="big-button link facebook external"><i class="icon ion-logo-facebook"></i>Facebook</a>
        <a href="http://twitter.com/share?text=Welcome%20To%20Yui&url=http://themeforest.net&hashtags=template,mobile" target="_blank" class="big-button link twitter external"><i class="icon ion-logo-twitter"></i>Twitter</a>
      </div>
      -->
    </div>
    <!--
    <div class="block">
      <div class="title-medium-container">
        <h2>Comments</h2>
      </div>

      <div class="list media-list comment-list no-hairlines">
        <ul>
          <li>
            <a href="#" class="item item-content">
              <div class="item-media"><img src="img/authors/author-7.jpg" alt=""></div>
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title">Elena Anka</div>
                  <div class="item-after"><i class="icon ion-ios-heart"></i>27</div>
                </div>
                <div class="item-text">Ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt.</div>
              </div>
            </a>
          </li>
          <li>
            <a href="#" class="item item-content">
              <div class="item-media"><img src="<?php echo base_url() ?>vendor/img/authors/author-2.jpg" alt=""></div>
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title">Zorka Ivka</div>
                  <div class="item-after"><i class="icon ion-ios-heart"></i>12</div>
                </div>
                <div class="item-text">Lorem ipsum dolor sit amet, consectetur.</div>
              </div>
            </a>
          </li>
          <li>
            <a href="#" class="item item-content">
              <div class="item-media"><img src="<?php echo base_url() ?>vendor/img/authors/author-1.jpg" alt=""></div>
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title">Camille Aline</div>
                  <div class="item-after"><i class="icon ion-ios-heart"></i>8</div>
                </div>
                <div class="item-text">Consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum.</div>
              </div>
            </a>
          </li>
        </ul>
      </div>

      <div class="title-medium-container title-small">
        <h2>New Comment</h2>
      </div>

      <div class="list no-hairlines custom-form">
        <ul>
          <li class="item-content item-input">
            <div class="item-inner">
              <div class="item-input-wrap">
                <input type="text" placeholder="Name">
                <span class="input-clear-button"></span>
              </div>
            </div>
          </li>
          <li class="item-content item-input">
            <div class="item-inner">
              <div class="item-input-wrap">
                <input type="email" placeholder="E-mail">
                <span class="input-clear-button"></span>
              </div>
            </div>
          </li>
          <li class="item-content item-input">
            <div class="item-inner">
              <div class="item-input-wrap">
                <textarea class="resizable" placeholder="Comment"></textarea>
                <span class="input-clear-button"></span>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <a class="big-button button button-fill link"><i class="icon ion-ios-send"></i>Post Comment</a>
    </div>
  -->
  </div>
</div>

<?php } ?>
