 <div id="view-categories" class="view tab <?php echo $this->session->flashdata('bg-pasangBaru-active') ?> animated fadeIn">
    <div data-name="categories" class="page no-navbar">
      <div class="navbar">
        <div class="navbar-inner sliding animated slideInDown">
          <div class="title">Permohonan Pasang Baru</div>
        </div>
      </div>
      <!-- Scrollable page content -->
      <div class="page-content">
        <!--<div class="discover-gradient">
          <svg viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="white" points="0,100 100,0 100,100"/></svg>
        </div> -->
        <!--<a href="#" class="link back close-button">
          <img src="<?php echo base_url() ?>vendor/img/close.svg" alt="Close">
        </a> -->
       <!-- <div class="oval"></div> -->
       <br><br>
       <p align="center"> <img src="<?php echo base_url() ?>vendor/img/pdam/pasang-baru.png" style=" width:100px; height:100px;"  alt="CS PDAM LANGSA"> </p>

       <div class="block animated fadeIn">

       
        
        <!-- FORM CEK TAGIHAN -->
        
        <div class="list no-hairlines custom-form contact-form">
          <form method="post" action="<?php echo base_url();?>index.php/welcome/addPasangBaru" enctype="multipart/form-data">
            <ul>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input required type="text" name="nama_pemohon" placeholder="Nama Pemilik / Calon Pelanggan">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input required type="number" name="ktp_pemohon" placeholder="ID KTP">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input required type="number" name="hp_pemohon" placeholder="No HP">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <select name="id_desa_pemohon" required>
                      <option value="">-Pilih Desa-</option>
                      <?php 
                      foreach($data_desa->result_array() as $d)
                      {
                        echo "<option value='".$d['DesaID']."'>".$d['Desa']."</option>";
                      }
                      ?>
                    </select>
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
             <!-- <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <select name="id_kec_pemohon" required>
                      <option value="">-Pilih Kecamatan-</option>
                      <?php 
                      foreach($data_kecamatan->result_array() as $d)
                      {
                        echo "<option value='".$d['KecamatanID']."'>".$d['Kecamatan']."</option>";
                      }
                      ?>
                    </select>
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li> -->
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input required type="text" name="alamat_pemohon" placeholder="Alamat Detail">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <br>
              Klik map dibawah untuk menandai lokasi alamat pemasangan:
               <p></p>
               <div id="googleMap_pemohon" 
                  style="
                    width:100%; 
                    height:350px; 
                    border-radius:10px; 
                    border: 0px solid black;">      
                </div>
                <input type="hidden" id="lat3" name="lat_pemohon" value="" required> 
                <input type="hidden" id="lng3" name="lng_pemohon" value="" required>
            </ul>
           
              <br>
              Lampiran KTP: <br>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                 
                    <input 
                      type="file" 
                      accept="image/*" 
                      name="img_ktp" 
                      capture="camera4" 
                      id="camera4">
                    <span class="input-clear-button"></span>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">           
                  <img id="frame4" width="100%" height="100%" src="<?php echo base_url() ?>vendor/img/pdam/preview.png" alt="Preview" style="border-radius: 40px;">
                  <?php echo $previewCamera4 ?> 
                </div>
              </li>
              <br>
            <hr>
            <button id="myButton4" data-loading-text="LOADING..." class="big-button button button-fill link animated fadeIn"><i class="icon ion-ios-send"></i>KIRIM</button>
          </form>        
        </div> 

      </div>
    <?php echo $preLoader ?>
      </div>
    </div>
  </div>