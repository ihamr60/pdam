<div id="view-status" class="view tab <?php echo $this->session->flashdata('bg-status-active') ?>">
  <div data-name="home" class="page no-navbar">
    <div class="navbar">
      <div class="navbar-inner sliding">
        <div class="left">
          <a href="#view-pages" class="link back tab-link">
          <i class="icon icon-back"></i>
          <span>Back</span>
          </a>
        </div>
        <div class="title">Check Status</div>
      </div>
    </div>
    <!-- Scrollable page content -->
    <div class="page-content">
      <div class="block animated fadeIn">
        
        <!-- FORM CEK TAGIHAN -->
        <div class="list no-hairlines custom-form contact-form">
          <form method="post" action="<?php echo base_url();?>index.php/welcome/cekStatus">
            <ul>
              <li class="item-content item-input">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input 
                      required 
                      type="number" 
                      placeholder="ID Report"
                      name="id_report">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input 
                      required 
                      type="number" 
                      placeholder="No HP"
                      name="hp_pelapor">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
            </ul>
            <button id="myButton2" data-loading-text="LOADING..." class="big-button button button-fill link animated fadeIn"><i class="icon ion-ios-send"></i>CHECK STATUS</button>
            <br/>
            <?php echo $preLoader ?>
          </form>
          <br>
            <p align="center"> PERHATIAN!!<br><br>Gunakan No Hp & ID Report yang diberikan saat melapor untuk memeriksa status pengaduan Anda saat ini</p>
          </div> 
          <div class="link-block promo-banner">
            <img src="<?php echo base_url() ?>vendor/img/pdam/admin.jpg" alt="CS PDAM LANGSA">
            <div class="link-infos">
              <font size="0">CS Officer</font>
              <div class="link-title"><font size="2"><b>Mr. Adit Raditya</b></font></div>
              <div class="link-url"><font size="2">+62 811-6721-483</font></div>
            </div>
            <a href="#" class="link"><img src="<?php echo base_url() ?>vendor/img/pdam/call.png" alt=""></a>
          </div>
         <!-- <br><br><br><br><br><br><br><br><br>
          <div class="block animated zoomInRight">      
            <p align="center"><img src="<?php echo base_url(); ?>vendor/img/pdam/repair.png"   style=" width:100px; height:100px;"></p>
            <h2 align="center">COMMING SOON</h2>
          </div> -->
       <!-- <div class="contact-container">
          <div class="contact-icon"><i class="icon ion-ios-mail-open"></i></div>
          <h1>Pasang Baru</h1>
          <div class="list no-hairlines custom-form contact-form">
            <ul>
              <li class="item-content item-input">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input required type="text" placeholder="ID Pelanggan">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <select required>
                      <option value="">Pilih Kategori</option>
                      <option value="Kategori A">Kategori A</option>
                      <option value="Kategori B">Kategori B</option>
                    </select>
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <textarea  required class="resizable" placeholder="Halo, ada yang bisa kami bantu?"></textarea>
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <a class="big-button button button-fill link"><i class="icon ion-ios-send"></i>Kirim Pengaduan</a>
        </div> -->
      </div>
    </div>
   <!-- <div class="page-content ptr-content infinite-scroll-content" data-infinite-distance="70">
      <div class="ptr-preloader">
        <div class="preloader"></div>
        <div class="ptr-arrow"></div>
      </div>
      <div class="block" id="today-content">
        <div class="title-container">
          <span class="title-date">Tuesday 19 March</span>
          <h1>Today</h1>
        </div>
        <a href="/single/">
          <div class="card">
            <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-1.jpg" alt="">
            <div class="card-infos">
              <div class="chip color-pink"><i class="icon ion-ios-flame"></i>Hot</div>
              <h2 class="card-title">Soprano Announces His New Album</h2>
              <div class="card-bottom">
                <div class="card-author">
                  <img class="card-author-image" src="<?php echo base_url() ?>vendor/img/authors/author-7.jpg" alt="">
                  <div>Elena Anka</div>
                </div>
                <div class="card-comments"><i class="icon ion-ios-text"></i>22</div>
              </div>
            </div>
          </div>
        </a>
        <a href="/single/">
          <div class="card card-style-2">
            <div class="card-image-container"><img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-2.jpg" alt=""></div>
            <div class="card-infos">
              <div class="card-category">Gaming</div>
              <h2 class="card-title">The Future of Gaming</h2>
              <p class="card-description">Mozilla has announced a new version of its browser for augmented reality</p>
            </div>
          </div>
        </a>
        <a href="/single/">
          <div class="card">
            <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-3.jpg" alt="">
            <div class="card-infos">
              <h2 class="card-title">Nicki Minaj in Featuring with Drake!</h2>
              <div class="card-bottom">
                <div class="card-author">
                  <img class="card-author-image" src="<?php echo base_url() ?>vendor/img/authors/author-3.jpg" alt="">
                  <div>Jess Roxana</div>
                </div>
                <div class="card-comments"><i class="icon ion-ios-text"></i>22</div>
              </div>
            </div>
          </div>
        </a>
        <div class="title-container">
          <span class="title-date">18 March</span>
          <h1>Monday</h1>
        </div>
        <div class="two-columns-cards">
          <a href="/single/">
            <div class="card">
              <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-4.jpg" alt="">
              <div class="card-infos">
                <h2 class="card-title">Nadal Wins at Roland Garros</h2>
              </div>
            </div>
          </a>
          <a href="/single/">
            <div class="card">
              <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-5.jpg" alt="">
              <div class="card-infos">
                <h2 class="card-title">NASA Chooses Its Next Chief Scientist</h2>
              </div>
            </div>
          </a>
          <a href="/single/">
            <div class="card">
              <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-6.jpg" alt="">
              <div class="card-infos">
                <h2 class="card-title">Top Sunglasses Models for This Summer</h2>
              </div>
            </div>
          </a>
          <a href="/single/">
            <div class="card">
              <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-7.jpg" alt="">
              <div class="card-infos">
                <h2 class="card-title">Isabel Marant Launches into Makeup!</h2>
              </div>
            </div>
          </a>
          <a href="/single/">
            <div class="card">
              <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-8.jpg" alt="">
              <div class="card-infos">
                <h2 class="card-title">Moto-Cross: The Champions Made the Show</h2>
              </div>
            </div>
          </a>
          <a href="/single/">
            <div class="card">
              <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-9.jpg" alt="">
              <div class="card-infos">
                <h2 class="card-title">The Best Exotic Destinations</h2>
              </div>
            </div>
          </a>
        </div>
        <div class="title-container">
          <span class="title-date">17 March</span>
          <h1>Sunday</h1>
        </div>
        <a href="/single/">
          <div class="card medium">
            <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-10.jpg" alt="">
            <div class="card-infos">
              <div class="card-date"><i class="icon ion-md-time"></i>2 days ago</div>
              <h2 class="card-title">Booba on Fire in His New Gotham Clip</h2>
            </div>
          </div>
        </a>
        <ul class="list media-list post-list" id="infinite-content">
          <li>
            <a href="/single/">
              <div class="item-content">
                <div class="item-media"><img src="<?php echo base_url() ?>vendor/img/thumb-11.jpg" alt=""></div>
                <div class="item-inner">
                  <div class="item-subtitle">Fashion</div>
                  <div class="item-title">The 6th Edition of the Body Painting Contest</div>
                  <div class="item-subtitle bottom-subtitle"><img src="<?php echo base_url() ?>vendor/img/authors/author-7.jpg" alt="">Elena Anka</div>
                </div>
              </div>
            </a>
          </li>
          <li>
            <a href="/single/">
              <div class="item-content">
                <div class="item-media"><img src="<?php echo base_url() ?>vendor/img/thumb-12.jpg" alt=""></div>
                <div class="item-inner">
                  <div class="item-subtitle">Photography</div>
                  <div class="item-title">20 Photography Tips for Taking Pictures</div>
                  <div class="item-subtitle bottom-subtitle"><img src="<?php echo base_url() ?>vendor/img/authors/author-3.jpg" alt="">Jess Roxana</div>
                </div>
              </div>
            </a>
          </li>
          <li>
            <a href="/single/">
              <div class="item-content">
                <div class="item-media"><img src="<?php echo base_url() ?>vendor/img/thumb-13.jpg" alt=""></div>
                <div class="item-inner">
                  <div class="item-subtitle">Beatles</div>
                  <div class="item-title">Tottenham: NFL Turf Is in Place!</div>
                  <div class="item-subtitle bottom-subtitle"><img src="<?php echo base_url() ?>vendor/img/authors/author-1.jpg" alt="">Camille Aline</div>
                </div>
              </div>
            </a>
          </li>
        </ul>
        <div class="preloader infinite-scroll-preloader"></div>
      </div>
    </div> -->
  </div>
</div>