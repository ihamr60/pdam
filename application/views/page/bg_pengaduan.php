<div id="view-formComplaint" class="view tab">
  <div data-name="pages" class="page">

    <!--<div class="navbar">
      <div class="navbar-inner sliding">
        <div class="title">Components</div>
      </div>
    </div> -->
    <!-- Scrollable page content -->
    <div class="page-content">
      <a href="#view-pages" class="link back close-button tab-link animated infinite pulse">
        <img src="<?php echo base_url() ?>vendor/img/close.svg" alt="Close">
      </a>
     <!-- <div class="oval"></div> -->
     <img class="" src="<?php echo base_url() ?>vendor/img/pdam/air2.jpg" height="250px" width="100%">
      <div class="block">
        <form method="post" action="<?php echo base_url();?>index.php/welcome/addComplaintNonPelanggan" enctype="multipart/form-data">
        <div class="contact-container">
          <div class="contact-icon"><img class="animated bounceIn" src="<?php echo base_url() ?>vendor/img/pdam/interface.png" height="50px" width="50px"></div>
          <h3 class="animated pulse">NON-PELANGGAN</h3>
          <div class="list no-hairlines custom-form contact-form">

          <p align="center"><font size="2"><b>Note:</b> Kami menjamin data pelapor tidak akan dipublikasikan. Pilih jenis pengaduan dibawah:</font></p>
            <ul>
              <?php echo $menuAtas_pengaduan ?>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input 
                      required 
                      type="number" 
                      placeholder="*NIK"
                      name="id_pelapor">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input 
                      required 
                      type="text" 
                      placeholder="*Nama Lengkap"
                      name="nama_pelapor">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input 
                      required 
                      type="number" 
                      placeholder="*Hp"
                      name="hp_pelapor">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input 
                       type="text" 
                       placeholder="Email (Optional)"
                       name="email_pelapor">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <select 
                      required
                      name="kateg_complaint">
                      <option value="">*Pilih Kategori</option>
                      <option value="Kebocoran Pipa">Kebocoran Pipa</option>
                      <option value="Bocor Dekat Meter">Bocor Dekat Meter</option>
                      <option value="Air Macet">Air Macet</option>
                      <option value="Meter Macet">Meter Macet</option>
                      <option value="Check Meteran">Check Meteran</option>
                      <option value="Keluhan Lainnya">Lainnya / Kritik & Saran</option>
                    </select>
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <textarea  
                      required 
                      class="resizable" 
                      placeholder="*Tuliskan detail keluhan / laporan Anda disini.."
                      name="isi_laporan"></textarea>
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
              Tandai Lokasi Gangguan:
               <p></p>
               <div id="googleMap_nonPelanggan" 
                  style="
                    width:100%; 
                    height:350px; 
                    border-radius:10px; 
                    border: 0px solid black;">      
                </div>
                <input type="hidden" id="lat2" name="lat_complaint" value="" required> 
                <input type="hidden" id="lng2" name="lng_complaint" value="" required>

                     <br>
                Lampiran: <br>
                <li class="item-content item-input animated fadeIn">
                  <div class="item-inner">
                   
                      <input 
                        type="file" 
                        accept="image/*" 
                        name="attach" 
                        capture="camera" 
                        id="camera">
                      <span class="input-clear-button"></span>
                  </div>
                </li>
                <li class="item-content item-input animated fadeIn">
                  <div class="item-inner">           
                    <img id="frame" width="100%" height="100%" src="<?php echo base_url() ?>vendor/img/pdam/preview.png" alt="Preview" style="border-radius: 40px">
                    <?php echo $previewCamera ?> 
                  </div>
                </li>
            </ul>
          </div>
          <button id="myButton3" data-loading-text="LOADING..." type="submit" class="big-button button button-fill link"><i class="icon ion-ios-send"></i>SUBMIT</button>
        </div>
        </form>
      </div>
    </div>
    <?php echo $preLoader ?>
  </div>
</div>
<script type="text/javascript">
  $("textarea").keydown(function(e){
    // Enter was pressed without shift key
    if (e.keyCode == 13 && !e.shiftKey)
    {
        // prevent default behavior
        e.preventDefault();
    }
    });
</script>