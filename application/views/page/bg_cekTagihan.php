<div id="view-today" class="view view-main tab <?php echo $this->session->flashdata('bg-cekTagihan-active') ?>">
  <div data-name="home" class="page no-navbar">
    <div class="navbar">
      <div class="navbar-inner sliding animated slideInDown">
        <div class="title">Check Tagihan</div>
      </div>
    </div>
    <!-- Scrollable page content -->
    <div class="page-content">
      <!--<div class="discover-gradient">
        <svg viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="white" points="0,100 100,0 100,100"/></svg>
      </div> -->
      <!--<a href="#" class="link back close-button">
        <img src="<?php echo base_url() ?>vendor/img/close.svg" alt="Close">
      </a> -->
     <!-- <div class="oval"></div> -->
     <br/><br/>
     <!--<div align="center">
        <img class="animated fadeIn" src="<?php echo base_url() ?>vendor/img/pdam/logo_pdam.png" width="300px">
     </div>-->
      <div class="block animated fadeIn">
        
        <!-- FORM CEK TAGIHAN -->
 
        <div class="list no-hairlines custom-form contact-form">
          <form method="post" action="<?php echo base_url();?>index.php/welcome/cekTagihanAPI">
            <ul>
              <li class="item-content item-input animated fadeIn">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input required type="number" name="id_pelanggan" placeholder="ID Pelanggan 5 Digit (Ex: 00303)"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "5">
                    <span class="input-clear-button"></span>
                  </div>
                </div>
              </li>
            </ul>
            <button class="big-button button button-fill link animated fadeIn"><i class="icon ion-ios-search"></i>SEARCH</button>
          </form>
          <p align="center">
            <font size="0">*Pastikan anda menginput ID Pelanggan <b>(5 Digit Angka Terakhir Pada Struk)</b></font>
          </p>
          
          <?php //echo $this->session->flashdata('infoTagihan9'); ?>

          <?php 
            for($i = 0 ; $i < 4000 ; $i++)
            {
              echo $this->session->flashdata('infoTagihan'.$i.'');
            } 
          ?>
          <p align="center"> 
            <img src="<?php echo base_url() ?>vendor/img/pdam/warning.png" height=80px alt="Attention">
            <font size='2'><b><u>PERHATIAN</u></b><br><br>**Membayar air lebih dari tanggal 20 akan dikenakan denda sebesar Rp. 7000. <br><br> **Jika tagihan anda tiba-tiba <b>MELONJAK</b>, maka sangat mungkin ada kebocoran pada jaringan pipa dirumah, periksa semuanya, dan lakukan pengaduan pada menu <b>"PENGADUAN"</b> yang tersedia dibawah. 
              </font>
              
          </p>
          <p align="center">
            <font size="2">
              <b>CARA MEMERIKSA KEBOCORAN:</b><br>
              1. Matikan semua keran air dirumah, pastikan tidak ada air yang mengalir.<br>
              2. Setelah dipastikan semua keran mati, amati meteran air.<br>
              3. Jika meteran tetap berputar, dapat dipastikan terjadi kebocoran pada jaringan pipa dirumah anda.<br><br>
              *Segera lakukan pengaduan!*
            </font>
          </p>

          <br><br><br>
          <div class="link-block promo-banner animated fadeIn">
            <img src="<?php echo base_url() ?>vendor/img/pdam/information.png" alt="CS PDAM LANGSA">
            <div class="link-infos">
              <div class="link-url"><font size="2"><?php echo $this->session->flashdata('last_updated') ?> Bayarlah Air sebelum tgl 20 (bln berjalan)</font></div>
            </div>
          </div>

        </div> 

  
  <br><br><br><br><br>
  <!--<p align="center">
    Mohon Maaf, fitur Cek Tagihan sedang dalam proses Maintenance / Perawatan Sistem.
  </p> -->

        
      </div>
    </div>
  </div>
</div>

<style>

/* The browser window */
.container {
  border: 3px solid #f1f1f1;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
}

/* Container for columns and the top "toolbar" */
.row {
  padding: 10px;
  background: #f1f1f1;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
}

/* Create three unequal columns that floats next to each other */
.column {
  float: left;
}

.left {
  width: 15%;
}

.right {
  width: 10%;
}

.middle {
  width: 75%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Three dots */
.dot {
  margin-top: 4px;
  height: 12px;
  width: 12px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
}

/* Three bars (hamburger menu) */
.bar {
  width: 17px;
  height: 3px;
  background-color: #aaa;
  margin: 3px 0;
  display: block;
}

/* Page content */
.content {
  padding: 10px;
}
</style>