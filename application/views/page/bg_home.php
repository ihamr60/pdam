<div id="view-discover" class="view tab <?php echo $this->session->flashdata('bg-home-active') ?> animated fadeIn">
    <div data-name="discover" class="page no-navbar">
      <!-- Scrollable page content -->
      <div class="page-content">
        <!--<div class="discover-gradient">
          <svg viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="white" points="0,100 100,0 100,100"/></svg>
        </div> -->
        <div class="block">
          <div class="title-container black">
            <span class="title-date">
              <?php
                  date_default_timezone_set("Asia/Jakarta");
                  
                  $b = time();
                  $jam = date("G",$b);

                  if ($jam>=0 && $jam<=11){
                    echo "Selamat Pagi ";
                  }
                  else if ($jam >=12 && $jam<=14){
                    echo "Selamat Siang";
                  }
                  else if ($jam >=15 && $jam<=17){
                    echo "Selamat Sore";
                  }
                  else if ($jam >=17 && $jam<=18){
                    echo "Selamat Petang";
                  }
                  else if ($jam >=19 && $jam<=23){
                    echo "Selamat Malam";
                  }
              ?> customer
            </span>
            <!--<div class="title-with-link">
              <h1>PDAM Langsa</h1>
              <i class="icon ion-ios-star"></i>
            </div> -->
          </div>
        </div> <!--
        <div id="discover-swiper" class="swiper-container">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <a href="#" data-href="/single/">
                <div class="card">
                  <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-36.jpg" alt="">
                  <div class="card-infos">
                    <h2 class="card-title">NASA Chooses Its Next Chief Scientist</h2>
                  </div>
                </div>
              </a>
            </div>
            <div class="swiper-slide">
              <a href="#" data-href="/single/">
                <div class="card">
                  <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-19.jpg" alt="">
                  <div class="card-infos">
                    <h2 class="card-title">Jeffrey Campbell's New Shoes Models</h2>
                  </div>
                </div>
              </a>
            </div>
            <div class="swiper-slide">
              <a href="#" data-href="/single/">
                <div class="card">
                  <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-41.jpg" alt="">
                  <div class="card-infos">
                    <h2 class="card-title">The Best Models of Sunglasses to Go out This Summer</h2>
                  </div>
                </div>
              </a>
            </div>
            <div class="swiper-slide">
              <a href="#" data-href="/single/">
                <div class="card">
                  <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-37.jpg" alt="">
                  <div class="card-infos">
                    <h2 class="card-title">Soprano Announces His New Album</h2>
                  </div>
                </div>
              </a>
            </div>
            <div class="swiper-slide">
              <a href="#" data-href="/single/">
                <div class="card">
                  <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-40.jpg" alt="">
                  <div class="card-infos">
                    <h2 class="card-title">Will Conor Return to the UFC Octagon?</h2>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div> -->
        <div class="block">
          <div class="title-medium-container title-with-link">
            <h2>Info Terkini:</h2>
            <a href="/author-list/" class="col button button-small button-round button-fill">Latest</a>
          </div>
          <ul class="popular-authors">
            <?php 
            if(!empty($info_gangguan))
            { 
              ?>
            <li>
              <a href="/author/" class="open-notification">
                <div class="author-image animated infinite tada"><img src="<?php echo base_url() ?>vendor/img/pdam/speaker.png" alt="Gangguan PDAM Langsa"></div>
                <div class="author-infos">
                  <div class="author-name"><font size="3"><?php echo $info_gangguan['judul_gangguan'] ?></font></div>
                  <div class="author-description"><i class="icon ion-md-time"></i> <?php echo date('d/m/Y H:i', strtotime($info_gangguan['gangguan_updated_at'])); ?> WIB<br>Klik selengkapnya</div>
                </div>
                <div class="author-star"><i class="icon ion-ios-warning animated infinite heartBeat"></i></div>
              </a>
            </li>
      <?php   
            } 
            else 
            { ?>
            <li>
              <a href="/author/">
                <div class="author-image animated infinite pulse"><img src="<?php echo base_url() ?>vendor/img/pdam/safe.png" alt="Safe PDAM Langsa"></div>
                <div class="author-infos">
                  <div class="author-description">Tidak ada indikasi gangguan</div>
                </div>
                <div class="author-star"><i class="icon ion-ios-star animated infinite pulse"></i></div>
              </a>
            </li>
      <?php } ?>
            <!--<li>
              <a href="/author/">
                <div class="author-image"><img src="<?php echo base_url() ?>vendor/img/authors/author-2.jpg" alt=""></div>
                <div class="author-infos">
                  <div class="author-name">Zorka Ivka</div>
                  <div class="author-description">Graphist and Webdesigner</div>
                </div>
                <div class="author-star"><i class="icon ion-ios-star"></i></div>
              </a>
            </li>
            <li>
              <a href="/author/">
                <div class="author-image"><img src="<?php echo base_url() ?>vendor/img/authors/author-1.jpg" alt=""></div>
                <div class="author-infos">
                  <div class="author-name">Camille Aline</div>
                  <div class="author-description">Fashion & Mode Worker</div>
                </div>
                <div class="author-star"><i class="icon ion-ios-star"></i></div>
              </a>
            </li>-->
          </ul>
          <div class="title-medium-container">
            <h2>PDAM Story of the Week</h2>
          </div>
        </div>
        <div id="discover-swiper2" class="swiper-container medium-card-slider style-2 animated fadeIn">
          <div class="swiper-wrapper">
            <?php 
              if(!empty($data_story))
              { 
                foreach($data_story->result_array() as $d) 
                { ?>
                  <div class="swiper-slide">
                    <a href="#view-post<?php echo $d['uuid'] ?>" data-toggle="modal">
                      <div class="card">
                        <img class="card-image" src="<?php echo base_url() ?>upload/artikel/<?php echo $d['img'] ?>" alt="">
                      </div>
                      <div class="card-infos">
                        <h2 class="card-title"><?php echo $d['judul'] ?></h2>
                      </div>
                    </a>
                  </div>
          <?php }
              } 
              else 
              { ?>
                  <div class="swiper-slide">
                    <a href="#" data-href="/single/">
                      <div class="card-infos">
                        <p class="card-title"><i class="icon ion-md-time"></i> Belum ada story</p>
                      </div>
                    </a>
                  </div>
        <?php } ?>
          </div>
        </div>
        <div class="block">
          <div class="title-medium-container title-with-link">
            <h2>Artikel seputar PDAM</h2>
            <a href="/category/" class="col button button-small button-round button-fill">Latest</a>
          </div>
          <ul class="list media-list post-list">
            <?php 
            if(!empty($data_tips))
            {  
              foreach($data_tips->result_array() as $d)
              { ?>
            <li>
              <a href="#view-post<?php echo $d['uuid'] ?>" class="tab-link">
                <div class="item-content">
                  <div class="item-media animated slideInLeft"><img src="<?php echo base_url() ?>upload/artikel/<?php echo $d['img'] ?>" alt=""></div>
                  <div class="item-inner">
                    <div class="item-subtitle animated jello"><?php echo $d['kategori_artikel'] ?></div>
                    <div class="item-title animated jello"><?php echo $d['judul'] ?></div>
                    <div class="item-subtitle bottom-subtitle animated jello"><i class="icon ion-md-time"></i><?php echo date('d/m/Y H:i', strtotime($d['artikel_updated_at'])); ?> WIB</div>
                  </div>
                </div>
              </a>
            </li>
          <?php }
            } 
            else 
            { ?>
            <li>
              <a href="#">
                <div class="item-content">
                  <div class="item-inner">
                    <div class="item-subtitle bottom-subtitle"><i class="icon ion-md-time"></i>Belum ada artikel</div>
                  </div>
                </div>
              </a>
            </li>
      <?php } ?>
          </ul>
          <!--<div class="title-medium-container">
            <h2>Must See</h2>
          </div> -->
        </div>
        <div id="overlay" onclick="off()">
          <div id="text"><img class="animated infinite pulse" src="<?php echo base_url() ?>vendor/img/pdam/hemat.jpg" width="345px" height="535px" style="border-radius: 20px"></div>
        </div>
        <?php echo $this->session->flashdata('pop-up') ?>
        <!--
        <div id="discover-swiper3" class="swiper-container">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <a href="#" data-href="/single/">
                <div class="card medium">
                  <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-28.jpg" alt="">
                  <div class="card-infos">
                    <div class="card-date"><i class="icon ion-md-time"></i>2 days ago</div>
                    <h2 class="card-title">Edmond at the Royal Palace Theatre</h2>
                  </div>
                </div>
              </a>
            </div>
            <div class="swiper-slide">
              <a href="#" data-href="/single/">
                <div class="card medium">
                  <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-42.jpg" alt="">
                  <div class="card-infos">
                    <div class="card-date"><i class="icon ion-md-time"></i>4 days ago</div>
                    <h2 class="card-title">Booba on Fire in His New Gotham Clip</h2>
                  </div>
                </div>
              </a>
            </div>
            <div class="swiper-slide">
              <a href="#" data-href="/single/">
                <div class="card medium">
                  <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-29.jpg" alt="">
                  <div class="card-infos">
                    <div class="card-date"><i class="icon ion-md-time"></i>3 weeks ago</div>
                    <h2 class="card-title">15 Cheap Outing Ideas in London</h2>
                  </div>
                </div>
              </a>
            </div>
            <div class="swiper-slide">
              <a href="#" data-href="/single/">
                <div class="card medium">
                  <img class="card-image" src="<?php echo base_url() ?>vendor/img/thumb-30.jpg" alt="">
                  <div class="card-infos">
                    <div class="card-date"><i class="icon ion-md-time"></i>1 month ago</div>
                    <h2 class="card-title">Ariana Grande Announces a New Fragrance</h2>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="block">
          <div class="title-medium-container">
            <h2>Popular Last Week</h2>
          </div>
          <ul class="list media-list post-list">
            <li>
              <a href="/single/">
                <div class="item-content">
                  <div class="item-media"><img src="<?php echo base_url() ?>vendor/img/thumb-31.jpg" alt=""></div>
                  <div class="item-inner">
                    <div class="item-subtitle">Fashion</div>
                    <div class="item-title">The Crocodile, Man's Best Friend?</div>
                    <div class="item-subtitle bottom-subtitle"><i class="icon ion-md-time"></i>4 hours ago</div>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="/single/">
                <div class="item-content">
                  <div class="item-media"><img src="<?php echo base_url() ?>vendor/img/thumb-32.jpg" alt=""></div>
                  <div class="item-inner">
                    <div class="item-subtitle">Science</div>
                    <div class="item-title">7 Accessories to Pack This Summer</div>
                    <div class="item-subtitle bottom-subtitle"><i class="icon ion-md-time"></i>6 days ago</div>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="/single/">
                <div class="item-content">
                  <div class="item-media"><img src="<?php echo base_url() ?>vendor/img/thumb-33.jpg" alt=""></div>
                  <div class="item-inner">
                    <div class="item-subtitle">Beatles</div>
                    <div class="item-title">Mylene Farmer Will Unveil Her New Single on Friday</div>
                    <div class="item-subtitle bottom-subtitle"><i class="icon ion-md-time"></i>Last week</div>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="/single/">
                <div class="item-content">
                  <div class="item-media"><img src="<?php echo base_url() ?>vendor/img/thumb-34.jpg" alt=""></div>
                  <div class="item-inner">
                    <div class="item-subtitle">Fashion</div>
                    <div class="item-title">Holidays Abroad: Leave Reassured!</div>
                    <div class="item-subtitle bottom-subtitle"><i class="icon ion-md-time"></i>Last month</div>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div>-->
      </div>
    </div>
  </div>



  <style>
#overlay {
  position: fixed;
  display: none;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  z-index: 2;
  cursor: pointer;
}

#text{
  position: absolute;
  top: 50%;
  left: 50%;
  font-size: 50px;
  color: white;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
}
</style>

  <script>
function on() {
  document.getElementById("overlay").style.display = "block";
}

function off() {
  document.getElementById("overlay").style.display = "none";
}
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.but').trigger('click');
    })
</script>