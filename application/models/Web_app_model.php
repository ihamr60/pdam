<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_App_Model extends CI_Model 
{	
	// db2 digunakan untuk mengakses database ke-2
	 private $db2;

	 public function __construct()
	 {
	  parent::__construct();
	         $this->db2 = $this->load->database('db2', TRUE);
	 }

	public function getLoginData($usr, $psw) 
	{
		$u = $usr; 
		$p = md5($psw);
		
		$q_cek_login = $this->db->get_where('app_login', array('username' => $u, 'password' => $p)); 
		if(count($q_cek_login->result())>0)
		{
			foreach ($q_cek_login->result() as $qck)
			{			
				if($qck->stts=='Administrator')
				{
					$q_ambil_data = $this->db->get_where('app_admin', array('username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->username;
						$sess_data['nama'] 		 	 = $qad->nama_lengkap;
						$sess_data['stts'] 			 = 'Administrator';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/panel/bg_home');
				}
			}
		}
		else
			{
				header('location:'.base_url().'index.php/panel');
				$this->session->set_flashdata('info','<div class="alert alert-danger">
					                                    <button data-dismiss="alert" class="close">
					                                        &times;
					                                    </button>
					                                    <i class="fa fa-times-circle"></i>
					                                    <strong>Ups, Maaf!</strong> Username atau password anda salah.
					                                </div>');
			}
	}

	public function insertData($data,$table)
	{
		$this->db->insert($table,$data);
	}

	public function getMaxIdReport($table,$order_by)
	{
		$data = array();
  		$Q = $this->db->query("SELECT id_report FROM ".$table." ORDER BY ".$order_by." DESC LIMIT 1");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getMaxIdReg($table,$order_by)
	{
		$data = array();
  		$Q = $this->db->query("SELECT id_reg FROM ".$table." ORDER BY ".$order_by." DESC LIMIT 1");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getMax($table,$order_by)
	{
		$data = array();
  		$Q = $this->db->query("SELECT * FROM ".$table." ORDER BY ".$order_by." DESC LIMIT 1");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getMaxIdWhere($table,$order_by,$where,$field_where)
	{
		$data = array();
  		$Q = $this->db->query("SELECT * FROM ".$table." WHERE ".$field_where."=".$where." ORDER BY ".$order_by." DESC LIMIT 1");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getWhereOneItem($where,$field,$table)
	{
		$data = array();
  		$options = array($field => $where);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getWhereTwoItem($where,$field,$where2,$field2,$table)
	{
		$data = array();
  		$options = array($field => $where, $field2 => $where2);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getAllData($table)
	{
		return $this->db->get($table);
		//return $this->db->get($table)->result();
	}

	public function updateDataWhere($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	public function getWhereAllItem($where,$field,$table)
	{
		$this->db->where($field,$where);
		return $this->db->get($table);
	}

	public function getWhereAllItem_between($where,$field,$table,$date_field,$tgl_awal,$tgl_akhir)
	{
		$this->db->where($field,$where);
		$this->db->where(''.$date_field.' >=',$tgl_awal);
		$this->db->where(''.$date_field.' <=',$tgl_akhir);
		return $this->db->get($table);
	}

	public function getAllData_between($table,$date_field,$tgl_awal,$tgl_akhir)
	{
		$this->db->where(''.$date_field.' >=',$tgl_awal);
		$this->db->where(''.$date_field.' <=',$tgl_akhir);
		return $this->db->get($table);
		//return $this->db->get($table)->result();
	}	

	public function getReport($year,$bulan,$stts)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM app_complaint WHERE year(complaint_updated_at)='".$year."' AND month(complaint_updated_at)='".$bulan."' AND stts_pelapor='".$stts."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getReportPasangBaru($year,$bulan,$stts)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM app_pasangBaru WHERE year(created_at_pasang_baru)='".$year."' AND month(created_at_pasang_baru)='".$bulan."' AND stts_pasang_baru='".$stts."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount($table,$field,$where)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field."='".$where."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount_betweenAllReport($table,$date_field,$tgl_awal,$tgl_akhir)
	{
		$data = array();
  		$Q = $this->db->query("select COUNT(*) AS TOTAL from ".$table." where ".$date_field." > '".$tgl_awal."' and ".$date_field."< '".$tgl_akhir."' ");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount_betweenReportLaporan($table,$date_field,$tgl_awal,$tgl_akhir,$stts_complaint,$stts)
	{
		$data = array();
  		$Q = $this->db->query("select COUNT(*) AS TOTAL from ".$table." where ".$date_field." > '".$tgl_awal."' and ".$date_field."< '".$tgl_akhir."' AND ".$stts_complaint." ='".$stts."' ");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount_betweenReportLaporan_2($table,$date_field,$tgl_awal,$tgl_akhir,$pic,$nm_pic,$pic2,$nm_pic2)
	{
		$data = array();
  		$Q = $this->db->query("select COUNT(*) AS TOTAL from ".$table." where ".$date_field." > '".$tgl_awal."' and ".$date_field."< '".$tgl_akhir."' AND ".$pic." ='".$nm_pic."' OR ".$date_field." > '".$tgl_awal."' and ".$date_field."< '".$tgl_akhir."' AND ".$pic2."='".$nm_pic2."' ");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount_betweenReportLaporan_pic($table,$date_field,$tgl_awal,$tgl_akhir,$stts_complaint,$stts,$pic,$nm_pic)
	{
		$data = array();
  		$Q = $this->db->query("select COUNT(*) AS TOTAL from ".$table." where ".$date_field." > '".$tgl_awal."' and ".$date_field."< '".$tgl_akhir."' AND ".$stts_complaint." ='".$stts."' AND ".$pic." ='".$nm_pic."' ");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount_betweenReportLaporan_pic_2($table,$date_field,$tgl_awal,$tgl_akhir,$stts_complaint,$stts,$pic,$nm_pic,$pic2,$nm_pic2)
	{
		$data = array();
  		$Q = $this->db->query("select COUNT(*) AS TOTAL from ".$table." where ".$date_field." > '".$tgl_awal."' and ".$date_field."< '".$tgl_akhir."' AND ".$stts_complaint." ='".$stts."' AND ".$pic." ='".$nm_pic."' OR ".$date_field." > '".$tgl_awal."' and ".$date_field."< '".$tgl_akhir."' AND ".$stts_complaint." ='".$stts."' AND ".$pic2."='".$nm_pic2."' ");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount2Where($table,$field1,$where1,$field2,$where2)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field1."='".$where1."' AND ".$field2."='".$where2."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount1Where($table,$field1,$where1)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field1."='".$where1."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function deleteData($table,$data)
	{
		$this->db->delete($table, $data);
	}

	public function getJoinAllWhere($idTabel1,$idTabel2,$table1,$table2,$field,$where)
	{
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getAll2Join($idTabel1,$idTabel2,$table1,$table2)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getAll3Join($idTabel1,$idTabel12,$idTabel2,$idTabel3,$table1,$table2,$table3)
	{
		 $this->db->select('*');
		 $this->db->from($table1);
		 $this->db->join($table2,''.$table2.'.'.$idTabel2.'='.$table1.'.'.$idTabel1.'');
		 $this->db->join($table3,''.$table3.'.'.$idTabel3.'='.$table1.'.'.$idTabel12.'');
		 $query = $this->db->get();
		 return $query;
	}

	public function getAll4Join($idTabel1,$idTabel12,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4)
	{
		 $this->db->select('*');
		 $this->db->from($table1);
		 $this->db->join($table2,''.$table2.'.'.$idTabel2.'='.$table1.'.'.$idTabel1.'');
		 $this->db->join($table3,''.$table3.'.'.$idTabel3.'='.$table1.'.'.$idTabel1.'');
		 $this->db->join($table4,''.$table4.'.'.$idTabel4.'='.$table1.'.'.$idTabel12.'');
		 $query = $this->db->get();
		 return $query;
	}



// DATABASE 2

	function allSumWhere_db2($field,$table,$field2,$where)
	{
		$data = array();
  		$Q = $this->db2->query("SELECT *, SUM(".$field.") as sum FROM ".$table." WHERE ".$field2."='".$where."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	function allSumWhere2_db2($field,$table,$field2,$where,$field3,$where2)
	{
		$data = array();
  		$Q = $this->db2->query("SELECT *, SUM(".$field.") as sum FROM ".$table." WHERE ".$field2."='".$where."' AND ".$field3."='".$where2."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	function getWhereAllItem_db2($where,$field,$table)
	{
		$this->db2->where($field,$where);
		return $this->db2->get($table);
	}

	public function getLimitOneItem_db2($var1,$var2,$table)
	{
		$data = array();
  		$Q = $this->db2->query("SELECT ".$var1." FROM ".$table." ORDER BY ".$var2." DESC LIMIT 1");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getWhereOneItem_db2($where,$field,$table)
	{
		$data = array();
  		$options = array($field => $where);
  		$Q = $this->db2->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}
}
